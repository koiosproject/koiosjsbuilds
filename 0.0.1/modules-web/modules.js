(function(global) {
/*
KoiosJS: A Cross-Platform Javascript Application Toolkit
www.koiosjs.com
*/


koios.asyncProps =  {
	url: "",
	method: "GET",
	timeout: 5000,
	body: null,
	headers: {},
	username: null,
	password: null,
	async: true,
	fields: {},
};

/*
KoiosJS: A Cross-Platform Javascript Application Toolkit
www.koiosjs.com
*/

koios.define({
	className: "koios.XHR",
	extend:"koios.Object",
	ignoreAlias: true,

	constructor: function() {
		koios.mixin(this, koios.asyncProps);
		this._super("_ct", arguments);
	},

	init: function() {
		this._super("init", arguments);
		this.xhr = this._makeXHR();
	},

// =================================================
// Private

	_makeXHR: function() {
		try {
			return new XMLHttpRequest();
		} catch(error) {
			koios.throw(error);
		}
	},

// =================================================
// Actions

	cancel: function() {
		this.emit("cancel", this.xhr);
		this.xhr.onreadystatechange = null;
		this.xhr = null;

		return this;
	},

	start: function() {
		if (this.username && this.username !== "") {
			this.xhr.open(this.method, this.url, this.async, this.username, this.password);
		} else {
			this.xhr.open(this.method, this.url, this.async);
		}

		this.emit("open", this.xhr);
		koios.mixin(this.xhr, this.fields);
		var t = this;

		this.xhr.onreadystatechange = function() {
			if (this.readyState === 4) {
				t.emit("data", this);
			}
		};

		if (!this.headers) {
			this.headers = {};
		}

		if (this.method === "GET") {
			this.headers["cache-control"] = "no-cache";
		}

		for (key in this.headers) {
			var value = this.headers[key];
			this.xhr.setRequestHeader(key, value);
		}

		this.xhr.timeout = this.timeout;
		this.xhr.ontimeout = function() {
			t.emit("timeout", this);
		}

		this.xhr.send(this.body);
		if (!this.async) {
			this.xhr.onreadystatechange(this.xhr);
		};

		return this;
	}
});
/*
KoiosJS: A Cross-Platform Javascript Application Toolkit
www.koiosjs.com
*/

koios.define({
	className:"koios.Ajax",
	extend:"koios.XHR",
	ignoreAlias: true,

	contentType: "application/x-www-form-urlencoded",
	useCache: false,
	responseType: "json",
	params: {},

	statics: {
		GET: function(args) {
			if (koios.isString(args)) {
				args = {url:args};
			}
			args.method = "GET";
			return koios.ajax(args);
		},
		POST: function(args) {
			if (koios.isString(args)) {
				args = {url:args};
			}
			args.method = "POST";
			return koios.ajax(args);
		},
		DELETE: function(args) {
			if (koios.isString(args)) {
				args = {url:args};
			}
			args.method = "DELETE";
			return koios.ajax(args);			
		},
		PUT: function(args) {
			if (koios.isString(args)) {
				args = {url:args};
			}
			args.method = "PUT";
			return koios.ajax(args);
		}
	},

	init: function() {
		this.promise = koios.promise();
		this._super("init", arguments);

		this.on("create", this.bind("start"));
	},

	start: function() {
		if (!this.url || this.url === "") {
			return koios.warn("koios.Ajax error: no URL");
		}

		var urlParts = this.url.split('?');
		var uri = urlParts.shift() || "";
		var args = koios.queryString("?"+urlParts.join('&'));
		if (!koios.isEmpty(this.params)) {
			args.add(this.params);
		}

		var callHeaders = {
			"Content-Type": this.contentType,
		};

		koios.mixin(this.headers, callHeaders);
		if (!this.useCache) {
			args.add("cache", koios.now());
		}

		if (!this.body || this.body == "") {
			this.body = args.toString();
		}

		if (this.method === "GET") {
			this.url = uri + args.toString();
		}


		this.on("timeout", this.bind("onTimeout"));
		this.on("data", this.bind("onData"));
		this._super("start");
	},

// =================================================
// Events

	onTimeout: function(xhr) {
		return this.promise.reject(new koios.Response({xhr:xhr, text:"timeout"}));
	},

	onData: function(xhr) {
		if (this._isFailure(xhr)) {
			return this.promise.reject(new koios.Response({xhr:xhr}));
		}
		var respType = this._contentType(xhr.getResponseHeader('Content-Type') || this.responseType.toLowerCase());
		var call = "_to"+koios.cap(respType);
		this[call](xhr);
	},

	_toJson: function(xhr) {
		this.promise.fulfill(new koios.Response({xhr:xhr, json:koios.JSON.parse(xhr.responseText)}));
		this.destroyLater();
	},

	_toXml: function(xhr) {
		this.promise.fulfill(new koios.Response({xhr:xhr, xml:xhr.responseXML}));
		this.destroyLater();
	},

	_toText: function(xhr) {
		this.promise.fulfill(new koios.Response({xhr:xhr}));
		this.destroyLater();
	},

// =================================================
// Private Manipulation

	_isFailure: function(xhr) {
		if (xhr.status === 0 && koios.isEmpty(xhr.responseText))
			return true;

		return (xhr.status !== 0) && (xhr.status < 200 || xhr.status >= 300);
	},

	_contentType: function(theHeader) { 
		if (!theHeader)
			return null;

		if (theHeader.toLowerCase().indexOf("json") > -1) {
			return "json";
		} else if (theHeader.toLowerCase().indexOf("xml") > -1) {
			return "xml";
		} else {
			return "text";
		}
	},

});

koios.ajax = function(args) {
	if (koios.isString(args)) {
		var args = {url:args};
	}

	var t = new koios.Ajax(args);
	return t.promise;
}
/*
KoiosJS: A Cross-Platform Javascript Application Toolkit
www.koiosjs.com
*/

koios.define({
	className:"koios.Response",
	extend:"koios.Class",
	ignoreAlias:true,

	init: function() {
		this._super("init", arguments);

		if (this.xhr) {
			this.text = !("text" in this) ? this.xhr.responseText : this.text;
			this.status = this.xhr.status;
		}
	},
});
/*
KoiosJS: A Cross-Platform JavaScript Application Framework
www.koiosjs.com
*/

koios.define({
    className:"koios.QueryString",
    extend:"koios.Class",
    alias:"koios.queryString",
    _stripHTML: false,
    
    constructor: function(args) {
        if (koios.isObject(args)) {
            this.theObject = args;
        } else if (koios.isString(args)) {
            this.theObject = {};
            this._parseString(args);
        }
        this._super("_ct");  
    },
    
// =========================================================================
// Query Manipulation

    add: function(theKey, theValue) {
        if (koios.isString(theKey)) {
            this.theObject[theKey] = theValue;
        } else if (koios.isObject(theKey)) {
            for (var n in theKey) {
                if (theKey.hasOwnProperty(n)) {
                    this.theObject[n] = theKey[n];
                }
            }
        }
        
        return this;
    },

    remove: function(theKey) {
        delete this.theObject[theKey];
        return this;
    },

    stripHTML: function() {
        this._stripHTML = true;
        return this;
    },

    toString: function(ignoreQueryStart) {
        var str = (ignoreQueryStart) ? "" : "?";
        for (var n in this.theObject) {
            if (this.theObject.hasOwnProperty(n)) {
                var val = this.theObject[n];

                val = encodeURIComponent(val);
                str += n+"="+val+"&";
            }
        }
       
        if (this._stripHTML) {
            // str = koios.stripHTML(str, true);
        }           
        str = str.substr(0, str.length-1);
        return str;
    },
    
// =========================================================================
// Private

    _parseString: function(theString) {
        var parts = theString.split("?");
        if (parts.length > 1) {
            var args = parts[1].split("&");
            for (var n=0;n<args.length;n++) {
                var argParts = args[n].split("=");
                this.theObject[argParts[0]] = argParts[1];
            }
        }
    }
});
/*
KoiosJS: A Cross-Platform JavaScript Application Framework
www.koiosjs.com
*/

koios.define({
	className:"koios.JSONPRequest",
	extend: "koios.Object",
	ignoreAlias:true,

	url: "",
	callbackName: "callback",
	charset: null,
	useCache: false,
	timeout: 1000,

	statics: {
		_id: 0,
		getID: function() {
			return this._id++;
		},
	},

	init: function() {
		this.promise = koios.promise();
		this._super("init", arguments);

		this.on("create", this.bind("start"));
	},

	start: function() {
		this.callback = "onJSONPReceive"+koios.JSONPRequest.getID();
		this.src = this._buildUrl();
		window[this.callback] = this.bind("onResponse");
		this._setupScript();
		koios.timeout(this.callback, this.bind("onTimeout"), this.timeout);
	},

// =================================================
// Script Events

	onError: function() {
		this.promise.reject("error");
		this._removeScript();
	},

	onResponse: function(response) {
		koios.clearTimeout(this.callback);
		this._removeScript();
		if (this.promise.state === koios.Promise.state.PENDING) {
			this.promise.fulfill(response);
		}
	},

	onTimeout: function() {
		if (this.promise.state === koios.Promise.state.PENDING) {
			this.promise.reject("timeout");
		}
		this._removeScript();
	},

// =================================================
// Private Building

	_buildUrl: function() {
		var parts = this.url.split("?");
		var uri = parts.shift() || "";
		var query = koios.queryString(this.url).add(this.callbackName, this.callback).stripHTML();
		if (!this.useCache) {
			query.add("cache", koios.now());
		}
		return uri+query.toString();
	},

	_setupScript: function() {
		this.scriptNode = document.createElement("script");
		this.scriptNode.setAttribute("src", this.src);
		this.scriptNode.setAttribute("async", "async");
		this.scriptNode.setAttribute("charset", (this.charset || "UTF-8"));

		this.scriptNode.onerror = this.bind("onError");
		document.getElementsByTagName('head').item(0).appendChild(this.scriptNode);
	},

	_removeScript: function() {
		document.getElementsByTagName('head').item(0).removeChild(this.scriptNode);
		this.scriptNode.onerror = null;
		this.scriptNode = null;	
		this.destroyLater();
	},
});

koios.jsonp = function(args) {
	if (koios.isString(args)) {
		args = {url:args};
	}

	var req = new koios.JSONPRequest(args);
	return req.promise;
};

/*
KoiosJS: A Cross-Platform JavaScript Application Framework
www.koiosjs.com
*/

_determineDbStrategy = function() {
	if (indexedDB) {
		return "koios.IndexedDB";
	} else if ("openDatabase" in this) {
		return "koios.WebSQL";
	} else {
		return "koios.LocalDB";
	}
}

koios.strategy.db = _determineDbStrategy();


// =================================================

/*
A koios.ClientDB is an in browser store of some form of data, backed by either:

	a) indexedDB (whenever possible)
	b) WebSQL (for mobile browsers & safari)
	c) localStorage - as a last ditch effort

It follows the following definition:

	koios.ClientDB.create({
		name:"TodoDB",
		
	})
*/

koios.define({
	className:"koios.ClientDB",
	extend: "koios.Object",

	init: function() {
		this._super("init", arguments);

		var name = koios.strategy.db;
		var ctor = koios.getConstructor(name);
		if (!koios.isEmpty(ctor)) {
			console.log("found db");
			// this.db = new ctor();
		} else {
			console.log("not found db");
		}
	},

});
/*
KoiosJS: A Cross-Platform JavaScript Application Framework
www.koiosjs.com
*/

koios.define({
	className:"koios.IndexedDB",
	extend: "koios.Class",
	inherit:"koios.EventSupport",
	ignoreAlias: true,

	dbName: "",
	version: 1,
	keyPath: "",

	constructor: function() {
		this.objectStores = [];
		this._storeNames = [];
		this._super(this._constructor, arguments);
	},

	init: function() {
		this._super("init", arguments);

		if (!this.dbName) {
			koios.throw("koios.IndexedDB error: give your IndexedDB a dbName");
		}

		for (var i=0;i<this.objectStores.length;i++) {
			var store = this.objectStores[i];
			var storeName;
			if (koios.isString(store)) {
				storeName = store;
			} else {
				storeName = store.name;
			}
			this._storeNames.push(storeName);
		}

		this.open();
	},

	open: function() {
		var request = indexedDB.open(this.dbName, this.version);
		var t = this;

		request.onupgradeneeded = this.bind("onUpgradeNeeded");

		request.onsuccess = function(e) {
			t.db = e.target.result;
			t.emit("open", t);
		};

		request.onerror = this.bind("onError");
	},

// =================================================
// Error Handling

	onError: function(error) {
		console.log(error);
		this.emit("error", error);
	},

	onSuccess: function(e) {
		this.emit("success", e);
	},

	onUpgradeNeeded: function(e) {
		var db = e.target.result;
		e.target.transaction.onerror = this.bind("onError");
		console.log("onupgradeneeded");

		for (var i=0;i<this.objectStores.length;i++) {
			var store = this.objectStores[i];
			console.log(store);
			var storeName;
			if (koios.isString(store)) {
				storeName = store;
			} else {
				storeName = store.name;
			}

			if (db.objectStoreNames.contains(storeName)) {
				db.deleteObjectStore(storeName);
			}
			if (this.keyPath && this.keyPath !== "") {
				var storeObj = db.createObjectStore(storeName, {keyPath:this.keyPath});
				if (store.indexes) {
					this._createIndexes(storeObj, store.indexes);
				}
			} else {
				var storeObj = db.createObjectStore(storeName, {autoIncrement: true});
				if (store.indexes) {
					this._createIndexes(storeObj, store.indexes);
				}
			}
		}
		this.onSuccess(e);
	},

// =================================================
// Private Manipulation

	_createIndexes: function(objectStore, indexes) {
		console.log("_createIndexes");
		console.log(indexes);
		for (var i=0;i<indexes.length;i++) {
			var index = indexes[i];
			console.log("creating index");
			console.log(index);
			var options = ("options" in index) ? index.options : {unique:false};
			objectStore.createIndex(index.name, index.fields, options);
		}
	},

	_objectStoreForName: function(storeName) {
		for (var i=0;i<this.objectStores.length;i++) {
			var store = this.objectStores[i];
			if (koios.isString(store)) {
				if (store === storeName) {
					return store;
				}
			} else if (koios.isObject(store)) {
				if (store.name === storeName) {
					return store;
				}
			}
 		}

 		return null;
	},

// =================================================
// Object Manipulation

	add: function(keyName, keyValue) {
		var objStore = this._objectStoreForName(keyName);
		if (!objStore)
			return koios.throw("koios.IndexedDB.add error: "+keyName+" not an available objectStore");

		var store = this.db.transaction([keyName], "readwrite").objectStore(keyName);
		if (!koios.isObject(keyValue)) {
			keyValue = {value:keyValue};
		};
		var request = store.put(keyValue);

		request.onsuccess = this.bind("onSuccess");
		request.onerror = this.bind("onError");
	},

	find: function(keyName, searchVal, callback) {
		var objStore = this._objectStoreForName(keyName);
		if (!objStore)
			return koios.throw("koios.IndexedDB.add error: "+keyName+" not an available objectStore");

		var store = this.db.transaction([keyName]).objectStore(keyName);
		var request;
		if (koios.isObject(searchVal)) {
			var keys = [], values = [];
			koios.object(searchVal).forEach(function(key, value) {
				keys.push(key);
				values.push(value);
			});

			var index = keys.join(",");
			if (objStore.indexes) {
				var filterdIdx = koios.array(objStore.indexes).filter(function(theItem) {
					return theItem.name === index;
				});
				if (filterdIdx.length === -12) {
					if (values.length > 1) {
						request = store.index(index).get(values);
					} else {
						request = store.index(index).get(values[0]);
					}
				} else {
					return this.findAndFilter(keyName, searchVal, callback);
				}
			}
		} else {
			request = store.get(searchVal);
		}

		request.onsuccess = function(e) {
			var result = event.target.result;
			callback(result);
		}

		request.onerror = this.bind("onError");
	},

	findAll: function(keyName, callback) {
		var objStore = this._objectStoreForName(keyName);
		if (!objStore)
			return koios.throw("koios.IndexedDB.add error: "+keyName+" not an available objectStore");

		var store = this.db.transaction([keyName]).objectStore(keyName);

		var keyRange = IDBKeyRange.lowerBound(0);
		var cursorRequest = store.openCursor(keyRange);
		var t = this;
		var results = [];

		cursorRequest.onsuccess = function(e) {
			var cursor = e.target.result;
			if (cursor) {
				results.push(cursor.value);
				cursor.continue();
			} else {
				if (callback) {
					callback(results);
				} else {
					t.emit("success", results);
				}
			}
		};

		cursorRequest.onerror = this.bind("onError");
	},

	findAndFilter: function(keyName, searchVal, callback) {
		var t = this;
		this.findAll(keyName, function(items) {
			var obj = items;
			for (key in searchVal) {
				var val = searchVal[key];
				obj = koios.array(obj).filter(function(theItem) {
					return theItem[key] === val;
				});
			}

			if (callback) {
				callback(obj);
			} else {
				t.emit("success", obj);
			}
		});
	},

	findOne: function(keyName, searchVal, callback) {
		var t = this;
		this.find(keyName, searchVal, function(items) {
			if (callback) {
				callback(items[0]);
			} else {
				t.emit("success", items[0]);
			}
		});
	},

	update: function() {
		// TODO
	},

	remove: function() {
		// TODO
	},

});
/*
KoiosJS: A Cross-Platform JavaScript Application Framework
www.koiosjs.com
*/

/*
An abstract class for handling the fact that not all apis responses are created equal
*/

koios.define({
	className:"koios.AjaxUnwrapper",
	extend:"koios.Class",

	onResponse: function(response) {
		return response.json;
	},
});
/*
KoiosJS: A Cross-Platform JavaScript Application Framework
www.koiosjs.com
*/

/*
An abstract class for handling the fact that not all apis responses are created equal
*/

koios.define({
	className:"koios.AjaxWrapper",
	extend:"koios.Class",

	onRequest: function(object) {
		return object;
	},
});
/*
KoiosJS: A Cross-Platform JavaScript Application Framework
www.koiosjs.com
*/

koios.define({
	className:"koios.RemoteDB",
	extend: "koios.Class",
	inherit:["koios.EventSupport"],

	accumulated:["adaptors"],
	adaptors:[],

	endpoints: {
		add: "",
		find: "",
		findAll: "",
		update: "",
		remove:"",
	},

	init: function() {
		this._super("init", arguments);

		var _adaptors = [];
		this._adaptors = {before:[], after:[]};

		this.modelCtor = koios.getConstructor(this.modelName);

		for (var i=0;i<this.adaptors.length;i++) {
			var theObject = this.adaptors[i];
            var theClass = theObject.className || this.defaultClass;

            theObject.className = (koios.isFunction(theClass)) ? theObject.prototype.className : theClass;

            theObject.owner = theObject.owner || this;
            var ctor = koios.getConstructor(theClass);
            if (!koios.isEmpty(ctor)) {
            	var obj = new ctor(theObject);
            	if (obj.onRequest) {
            		this._adaptors.before.push(obj);
            	} 
            	if (obj.onResponse) {
            		this._adaptors.after.push(obj);
            	}
            }
		}
	},

// =================================================
// Private

	_ajax: function(endpoint, params, options, method, onSuccess, onError) {
		var args = {};
		if (!("append" in options) || options.append) {
			args = params;
		}

		koios.ajax({
			url:endpoint,
			params:args,
			method:method,
		}).then(function(response) {
			onSuccess(response);
		}, function(error) {
			onError(error);
		});
	},

	_endpoint: function(action, callback) {
		var endpointInfo = this.endpoints[action];
		var action, endpoint;
		var options = {};
		var _actions = ["GET", "POST", "PUT", "DELETE"];
		koios.object(endpointInfo).forEach(function(key, value) {
			if (koios.indexOf(_actions, key) > -1) {
				action = key;
				endpoint = value;
			} else if (key === "options") {
				options = value;
			}
		});

		if (endpoint) {
			callback(action, endpoint, options);
		}
	},

	_formatBefore: function(theObject) {
		koios.each(this._adaptors.before, function(adaptor) {
			theObject = adaptor.onRequest(theObject);
		});
		return theObject;
	},

	_formatAfter: function(theResponse) {
		koios.each(this._adaptors.after, function(adaptor) {
			theResponse = adaptor.onResponse(theResponse);
		});

		return theResponse;
	},

	_handle: function(endpoint, theItem, onSuccess, onError) {
		var t = this;
		t._endpoint(endpoint, function(action, endpoint, options) {
			var obj = (theItem && "toObject" in theItem) ? theItem.toObject() : theItem;
			if (obj) {
				obj = t._formatBefore(obj);
			} else {
				obj = {};
			}

			endpoint = koios.template(endpoint, obj);
			t._ajax(endpoint, obj, options, action.toUpperCase(), function(response) {
				response = t._formatAfter(response);
				onSuccess(response);
			}, function(error) {
				t.emit("error", error);
				onError(error);
			});
		});
	},

// =================================================

	fetch: function() {

	},

// =================================================
// Store Endpoints

	findAll: function(onSuccess, onError) {		
		var t = this;
		t._handle("findAll", {}, function(response) {
			if (!koios.isArray(response)) {
				response = [response];
			}

			var objs = [];
			var ctor = t.modelCtor;
			koios.each(response, function(respObj) {
				objs.push(new ctor(respObj));
			});

			if (onSuccess) {
				onSuccess(objs);
			}
		}, function(error) {
			if (onError) {
				onError(t, error, theItem);
			}
		});
	},

	// add: function(theItem, onSuccess, onError) {
	// 	var t = this;
	// 	this._endpoint("add", function(action, endpoint, options) {
	// 		endpoint = koios.template(endpoint, theItem.toObject());
	// 		t._ajax(endpoint, theItem.toObject(), options, action.toUpperCase(), function(response) {
	// 			theItem._storeRef.push(t);
	// 			t.emit("save", theItem);
	// 			if (onSuccess) {
	// 				onSuccess(t, theItem);
	// 			}
	// 		}, function(error) {
	// 			t.emit("error", error);
	// 			if (onError) {
	// 				onError(t, error, theItem);
	// 			}
	// 		});
	// 	});
	// },

	// find: function(theSearch, onSuccess, onError) {
	// 	var t = this;
	// 	this._endpoint("find", function(action, endpoint, options) {
	// 		endpoint = koios.template(endpoint, theSearch);
	// 		t._ajax(endpoint, theSearch, options, action.toUpperCase(), function(response) {
	// 			koios.each(t._adaptors.after, function(adaptor) {
	// 				response = adaptor.onResponse(response);
	// 			});

	// 			if (onSuccess) {
	// 				onSuccess(response);
	// 			}
	// 		}, function(error) {
	// 			if (onError) {
	// 				onError(error);
	// 			}
	// 		});
	// 	});
	// },

	// findAll: function(onSuccess, onError) {
	// 	var t = this;
	// 	this._endpoint("findAll", function(action, endpoint, options) {
	// 		t._ajax(endpoint, {}, options, action.toUpperCase(), function(response) {
	// 			koios.each(t._adaptors.after, function(adaptor) {
	// 				response = adaptor.onResponse(response);
	// 			});
				
	// 			if (onSuccess) {
	// 				onSuccess(response);
	// 			}
	// 		}, function(error) {
	// 			if (onError) {
	// 				onError(error);
	// 			}
	// 		});
	// 	});
	// },

	// findOne: function(theSearch, onSuccess, onError) {
	// 	var t = this;
	// 	t.find(theSearch, function(response) {
	// 		if (koios.isArray(response)) {
	// 			if (onSuccess) {
	// 				onSuccess(response[0]);
	// 			}
	// 		} else {
	// 			if (onSuccess) {
	// 				onSuccess(response);
	// 			}
	// 		}
	// 	}, onError);
	// },

	// remove: function(theItem, onSuccess, onError) {
	// 	var t = this;
	// 	this._endpoint("remove", function(action, endpoint, options) {
	// 		endpoint = koios.template(endpoint, theItem.toObject());
	// 		t._ajax(endpoint, theItem.toObject(), options, action.toUpperCase(), function(response) {
	// 			t.emit("remove", theItem);
	// 			if (onSuccess) {
	// 				onSuccess(t, theItem);
	// 			}
	// 		}, function(error) {
	// 			t.emit("error", error);
	// 			if (onError) {
	// 				onError(t, error, theItem);
	// 			}
	// 		});
	// 	});
	// },

	// update: function(theItem, onSuccess, onError) {
	// 	var t = this;
	// 	this._endpoint("update", function(action, endpoint, options) {
	// 		endpoint = koios.template(endpoint, theItem.toObject());
	// 		t._ajax(endpoint, theItem.toObject(), options, action.toUpperCase(), function(response) {
	// 			t.emit("update", theItem);
	// 			if (onSuccess) {
	// 				onSuccess(t, theItem);
	// 			}
	// 		}, function(error) {
	// 			t.emit("error", error);
	// 			if (onError) {
	// 				onError(t, error, theItem);
	// 			}
	// 		});
	// 	});
	// },
});

koios.Store._typeMap["remote"] = koios.RemoteDB;

/*
KoiosJS: A Cross-Platform Javascript Application Toolkit
www.koiosjs.com
*/

/*jslint sloppy:true*/
/*jslint nomen:true*/
/*global koios, module*/

koios.query = function (theSearch, useFirst) {
    var results = document.querySelectorAll(theSearch),
        arr = Array.prototype.slice.call(results, 0);
    
    if (arr.length === 0) {
        return null;
    }
    
    if (useFirst) {
        return arr[0];
    } else {
        return koios.dom(arr);
    }
};

/*
KoiosJS: A Cross-Platform Javascript Application Toolkit
www.koiosjs.com
*/

/*jslint sloppy:true*/
/*jslint nomen:true*/
/*global koios, module*/

var _node = function (theNode) {
    if (koios.isString(theNode)) {
        return koios.query(theNode, true);
    } else {
        return theNode;
    }
},
    
    _styleToObject = function (css) {
        var ret = {};
        
        if (!css) {
            return ret;
        }
        
        koios.each(css.split(";"), function (theItem) {
            var pairs = theItem.split(":"),
                key,
                value;
            
            if (pairs.length > 1) {
                key = koios.string(pairs[0]).trimWhitespace();
                value = koios.string(pairs[1]).trimWhitespace();
                ret[key] = value;
            }
        });
        
        return ret;
    },
    
    _objectToStyle = function (theObject) {
        var css = "";
        
        koios.object(theObject).forEach(function (theKey, theValue) {
            css += theKey + ":" + theValue + ";";
        });
        
        return css;
    };

koios.mixin(koios, {
    
// =================================================
// Attributes
    
    attr: function (theNode, theAttribute, theValue) {
        if (arguments.length === 3) {
            return koios.setAttr(theNode, theAttribute, theValue);
        } else {
            return koios.getAttr(theNode, theAttribute);
        }
    },
    
    setAttr: function (theNode, theAttribute, theValue) {
        var node = _node(theNode);
        if (!node) {
            return null;
        }
        
        if (theValue) {
            node.setAttribute(theAttribute, theValue);
        } else {
            return koios.getAttr(theNode, theAttribute);
        }
        
        return node;
    },
        
    getAttr: function (theNode, theAttribute) {
        var node = _node(theNode);
        
        if (node) {
            return node.getAttribute(theAttribute);
        } else {
            return null;
        }
    },
    
    removeAttr: function (theNode, theAttribute) {
        var node = _node(theNode);
        
        if (node) {
            return node.removeAttribute(theAttribute);
        } else {
            return null;
        }
    },

// =================================================
// CSS Classes
    
    allClasses: function (theNode) {
        var node = _node(theNode),
            attr = koios.attr(theNode, "class");
        
        if (node && attr) {
            return attr.split(" ");
        } else {
            return [];
        }
    },
    
    hasClass: function (theNode, theClass) {
        var attr = koios.allClasses(theNode);
        return koios.indexOf(attr, theClass) > -1;
    },
    
    addClass: function (theNode, theClass) {
        var node = _node(theNode),
            classes;
        
        if (node) {
            classes = koios.allClasses(theNode);
            classes.push(theClass);
            koios.attr(theNode, "class", classes.join(" "));
        }
    },
      
    removeClass: function (theNode, theClass) {
        var node = _node(theNode),
            classes = koios.allClasses(theNode);
        
        if (node && classes) {
            classes.splice(koios.indexOf(classes, theClass), 1);
            
            if (classes.length > 0) {
                koios.attr(node, "class", classes.join(" "));
            } else {
                koios.removeAttr(node, "class");
            }
        }
        
        return node;
    },
    
    toggleClass: function (theNode, theClass) {
        var node = _node(theNode),
            hasClass = koios.hasClass(theNode, theClass);
        
        if (node) {
            if (hasClass) {
                koios.removeClass(node, theClass);
            } else {
                koios.addClass(node, theClass);
            }
        }
        
        return node;
    },

// =================================================
// CSS Classes
    
    cssToObject: function (theNode) {
        var node = _node(theNode),
            css;
        
        if (node) {
            css = koios.attr(node, "style");
            return _styleToObject(css);
        } else {
            return {};
        }
    },
    
    css: function (theNode, theKey, theValue) {
        if (arguments.length === 1) {
            return koios.attr(theNode, "style");
        }
        
        var css = koios.cssToObject(theNode);
        
        if (koios.isString(theKey) && arguments.length === 2) {
            return css[theKey];
        }
        
        if (koios.isString(theKey)) {
            css[theKey] = theValue;
        } else {
            koios.mixin(css, theKey);
        }
        
        koios.attr(theNode, "style", _objectToStyle(css));
    },
    
    removeCss: function (theNode, theKey) {
        var css = koios.cssToObject(theNode);
        
        delete css[theKey];
        return koios.attr(theNode, "style", _objectToStyle(css));
    },
    
    show: function (theNode) {
        koios.removeClass(theNode, "koios-hidden");
    },
    
    hide: function (theNode) {
        koios.addClass(theNode, "koios-hidden");
    },
    
    toggleHidden: function (theNode) {
        koios.toggleClass(theNode, "koios-hidden");
    },
    
    invisible: function (theNode) {
        koios.addClass(theNode, "koios-invisible");
    },
    
    visible: function (theNode) {
        koios.removeClass(theNode, "koios-invisible");
    },
    
    toggleVisible: function (theNode) {
        koios.toggleClass(theNode, "koios-invisible");
    },
    
// =================================================
// DOM Elements & HTML
    
    el: function (elName, params) {
        if (arguments.length < 2) {
            params = {};
        }

        var el = document.createElement(elName),
            key;

        for (key in params) {
            if (params.hasOwnProperty(key)) {
                el.setAttribute(key, params[key]);
            }
        }
        
        return el;
    },
    
    html: function (theNode, theHTML) {
        var node = _node(theNode);
        if (node && theHTML) {
            node.innerHTML = theHTML;
        }
        
        if (arguments.length < 2) {
            return node.innerHTML;
        } else {
            return node;
        }
    },

// =================================================
// Positioning & Sizing
    
    scrollTop: function (theNode) {
        var node = _node(theNode);
        if (node) {
            if (node.hasOwnProperty("scrollTop")) {
                return node.scrollTop;
            } else {
                return node.scrollY;
            }
        } else {
            return null;
        }
    },
    
    position: function (theNode) {
    },
    
    frame: function (theNode) {
    },
    
    size: function (theNode) {
    },
    
// =================================================
// Nodes
    
    append: function (theNode, theAppend) {
        var node = _node(theNode),
            innerHTML = "";
        
        if (node) {
            if (koios.isString(theAppend)) {
                innerHTML = node.innerHTML + theAppend;
                koios.html(node, innerHTML);
            } else {
                node.appendChild(theAppend);
            }
        }
            
        return node;
    },
      
    remove: function (theNode, theRemove) {
        var node = _node(theNode),
            innerHTML = "";
        
        if (node) {
            if (koios.isString(theRemove)) {
                innerHTML = koios.html(node).replace(theRemove, "");
                koios.html(node, innerHTML);
            } else {
                if (koios.containsNode(node, theRemove)) {
                    node.removeChild(theRemove);
                }
            }
        }
    },
    
    containsNode: function (theNode, theContains) {
        var node = _node(theNode);
        
        if (node) {
            return node.contains(theContains);
        } else {
            return false;
        }
    },
    
    childNodes: function (theNode) {
        var node = _node(theNode);
        
        if (node) {
            return node.childNodes;
        } else {
            return [];
        }
    },
    
    firstChild: function (theNode) {
        var children = koios.childNodes(theNode);
        if (children.length > 0) {
            return children[0];
        } else {
            return null;
        }
    },
    
    lastChild: function (theNode) {
        var children = koios.childNodes(theNode);
        if (children.length > 0) {
            return children[children.length - 1];
        } else {
            return null;
        }
    }

    
});

/*
KoiosJS: A Cross-Platform Javascript Application Toolkit
www.koiosjs.com
*/

/*jslint sloppy:true*/
/*jslint nomen:true*/
/*global koios, module*/
    
koios._domWrapper = koios.mixin({}, koios._arrayWrapper);

var _domItems = ["attr", "setAttr", "removeAttr", "getAttr", "allClasses", "hasClass", "addClass", "removeClass", "toggleClass", "containsNode", "childNodes", "firstChild", "lastChild", "html", "append", "remove", "containsNode", "css", "removeCss", "on", "off", "scrollTop", "position", "frame", "show", "hide", "visible", "invisible", "toggleHidden", "toggleVisible"],

    _mkfn = function (theMethod) {
        return function (args1, args2) {
            return this.map(function (theItem) {
                
                return koios[theMethod](theItem, args1, args2);
            });
        };
    },

    setDomItems = function () {
        var i, key;
        for (i = 0; i < _domItems.length; i += 1) {
            key = _domItems[i];
            koios._domWrapper[key] = _mkfn(key);
        }
    };

setDomItems();

koios.dom = function (theObject) {
    return new koios.wrapper(theObject, koios._domWrapper);
};


})(this);
