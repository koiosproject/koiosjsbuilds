(function(global) {
/*
KoiosJS: A Cross-Platform Javascript Application Toolkit
www.koiosjs.com
*/

/*global koios*/

if (typeof koios === "undefined") {
    var em = {}

    koios = {
        toString: function () {
            return "KoiosJS \n\nVersion Details: \n"+koios.JSON.stringify(koios.version);
        },
        ENV: {},
        JSON: JSON,
        hasProp: function(owner, prop) {
            Object.prototype.hasOwnProperty.call(owner, prop);
        },
        mixin: function(target, source) {
            if (source) {
                for (var key in source) {
                    if (!koios.hasProp(source, key)) {
                        var value = source[key];
                        if (em[key] !== value) {
                            target[key] = value;
                        }
                    }
                }
            }
            return target;
        }
    }
}

if (typeof module !== "undefined" && module.exports) {
    module.exports = koios;
} else {
    window.koios = koios;
}

/*
KoiosJS: A Cross-Platform Javascript Application Toolkit
www.koiosjs.com
*/

_objToString = function(obj) {
    return Object.prototype.toString.call(obj);
};

koios.mixin(koios, {
    isFunction: function(theFunction) {
        return typeof theFunction === "function";
    },

    isObject: function(theObject) {
        return typeof theObject === "object" && _objToString(theObject) !== "[object Array]";
    },

    isArray: function(theObject) {
        return _objToString(theObject) === "[object Array]";
    },

    isNumber: function(theObject) {
        return _objToString(theObject) === "[object Number]";
    },

    isString: function(theObject) {
        return _objToString(theObject) === "[object String]";
    },

    isDate: function(theObject) {
        return _objToString(theObject) === "[object Date]";
    },

    isArguments: function(theObject) {
        return _objToString(theObject) === "[object Arguments]";
    },

    isBoolean: function(theObject) {
        return _objToString(theObject) === "[object Boolean]";
    },

    isEvent: function(theEvent) {
        var s = theEvent.toString();
        var ptr = /\[object (Keyboard|Mouse|Focus|Wheel|Composition|Storage)?Event[s]?\]/
        return ptr.test(s);
    },

    isRegex: function(theObject) {
        return theObject instanceof RegExp;
    },

// =================================================
// Existence Checking

    exists: function(theObject) {
        return typeof theObject !== "undefined";
    },

    isNaN: function(theObject) {
        return theObject !== theObject;
    },

    isEmpty: function(theObject) {
        if (!theObject) {
            return true;
        } else if (koios.isString(theObject) || koios.isArguments(theObject) || koios.isArray(theObject)) {
            return theObject.length === 0;
        } else if (koios.isNumber(theObject)) {
            return theObject === 0;
        } else if (koios.isObject(theObject)) {
            var keys = koios.object(theObject).keys();
            return keys.length === 0;
        } else {
            return false;
        }
    },

    isFalseish: function(theObject) {
        return (theObject) ? true : false;
    },

// =================================================
// Koios OO type checking

    isa: function(theObject, theClass) {
        return (theObject && theObject.isa && theObject.isa(theClass));
    },

    isaSubclassOf: function(theObject, theClass) {
        return (theObject && theObject.isaSubclassOf && theObject.isaSubclassOf(theClass));
    },
});

/*
KoiosJS: A Cross-Platform Javascript Application Toolkit
www.koiosjs.com
*/

/*jslint nomen:true*/
/*global koios*/

koios.mixin(koios, {
    _queue: {},
    _tos: {},

    defer: function (theCallback) {
        if (typeof process === "object" && process.nextTick) {
            process.nextTick(theCallback);
        } else if (typeof setImmediate === "function") {
            setImmediate(theCallback);
        } else {
            setTimeout(theCallback, 0);
        }
    },

    wait: function (theQueueName, theCallback, theTimeout) {
        koios.stop(theQueueName);
        koios._queue[theQueueName] = setTimeout(function () {
            koios.stop(theQueueName);
            theCallback();
        }, theTimeout);
    },

    throttle: function (theQueueName, theCallback, theTimeout) {
        if (koios._queue[theQueueName]) {
            return;
        }

        theCallback();

        koios._queue[theQueueName] = setTimeout(function () {
            koios.stop(theQueueName);
        }, theTimeout);
    },

    stop: function (theQueueName) {
        if (koios._queue[theQueueName]) {
            clearTimeout(koios._queue[theQueueName]);
            koios._queue[theQueueName] = null;
        }
    },

    timeout: function (theName, theCallback, theTimeout) {
        koios._tos[theName] = setTimeout(function () {
            theCallback();
            koios.clearTimeout(theName);
        }, theTimeout);
    },

    clearTimeout: function (theName) {
        clearTimeout(koios._tos[theName]);
        koios._tos[theName] = null;
    }
});

/*
KoiosJS: A Cross-Platform Javascript Application Toolkit
www.koiosjs.com
*/

/*global koios*/

koios.clone = function (theSrc, theOffset, theStarting) {
    var obj = {},
        key,
        i;
    
    if (theOffset === null) {
        theOffset = 0;
    }
    if (theStarting === null) {
        theStarting = [];
    }

    if (!koios.exists(theSrc) || typeof theSrc !== "object" || koios.isFunction(theSrc)) {
        return theSrc;
    }

    if (koios.isDate(theSrc)) {
        return new Date(theSrc.getTime());
    }

    if (theSrc.hasOwnProperty("nodeType") && theSrc.hasOwnProperty("cloneNode")) {
        return theSrc.cloneNode(true);
    }

    if (theSrc instanceof RegExp) {
        return new RegExp(theSrc);
    }

    if (koios.isArray(theSrc) || koios.isArguments(theSrc)) {
        for (i = theOffset; i < theSrc.length; i += 1) {
            theStarting.push(theSrc[i]);
        }
        return theStarting;
    }

    if (koios.isObject(theSrc)) {
        for (key in theSrc) {
            if (theSrc.hasOwnProperty(key)) {
                obj[key] = theSrc[key];
            }
        }
        return obj;
    }

    return theSrc;
};

/*
KoiosJS: A Cross-Platform Javascript Application Toolkit
www.koiosjs.com
*/

/*jslint nomen:true*/
/*global koios*/

var _configDefaults = {
	logLevel: 5,  // koios.Logger.LOG
	showWindowAlerts: true,
	warnEmptyCalls: false,
	warnFrozenProperties: true,
	templateStyle: "es6"
};

koios.ENV.config = {};

koios.mixin(koios, {
    config: function (paramName, paramValue) {
        if (arguments.length === 0) {
            return koios.ENV.config;
        }
        
        if (arguments.length === 1) {
            if (koios.isString(paramName)) {
                return koios.ENV.config[paramName];
            } else if (koios.isObject(paramName)) {
                koios.mixin(koios.ENV.config, paramName);
                return koios.ENV.config;
            }

            return koios.warn("koios.config() error: Unknown paramName type");
        }

        koios.ENV.config[paramName] = paramValue;
        return paramValue;
    },

    _version: {
        "koios": "0.0.3pre"
    },

    version: function () {
        return koios._version.koios;
    },

    modules: function () {
        var keys = koios.object(koios.version).keys();
        koios.array(keys).filter(function (theItem) {
            return theItem !== "koios";
        });
    },

    hasModule: function (theModule) {
        return koios.version.hasOwnProperty(theModule);
    }
});

koios.config(_configDefaults);

/*
KoiosJS: A Cross-Platform Javascript Application Toolkit
www.koiosjs.com
*/

koios.mixin(koios, {
    bind: function(theScope, theMethod) {        
        var args = [theScope].concat(Array.prototype.slice.call(arguments, 2));
        
        if (arguments.length === 1) {
            theMethod = theScope;
            theScope = this;
        }
        
        if (koios.isString(theMethod)) {
            theMethod = theScope[theMethod];
        }
        
        return Function.prototype.bind.apply(theMethod, args);
    },

    once: function(theFn) {
        var exec = false;
        var res = null;
        return function() {
            if (exec) {
                return res;
            }
            exec = true;
            res = theFn.apply(this, arguments);
            theFn = null;
            return res;
        }
    },

    wrap: function(theFn, theCallback) {
        return function() {
            var args = [theFn].concat(koios.argumentsToArray(arguments));
            theCallback.apply(this, args);
        }
    },

    memoize: function(theFn) {
        if (!theFn.memoize)
            theFn.memoize = {};

        return function() {
            var args = Array.prototype.slice.call(arguments);
            var hash = "";
            var i = args.length;
            var currentArg = null;
            while(i--) {
                currentArg = args[i];
                hash += (currentArg === Object(currentArg)) ? JSON.stringify(currentArg) : currentArg;
            }

            return (hash in theFn.memoize) ? theFn.memoize[hash] : theFn.memoize[hash] = fn.apply(this, args);
        }
    },

    deprecated: function(fnName, replacement) {
        koios.warn(fnName+" is deprecated - use "+replacement+" instead.");
    },
});

/*
KoiosJS: A Cross-Platform Javascript Application Toolkit
www.koiosjs.com
*/

/*jslint nomen:true*/
/*global koios*/

koios.mixin(koios, {
    rand: function (theBounds) {
        if (arguments.length === 0) {
            theBounds = 1;
        }

        return Math.floor(Math.random() * theBounds);
    },

    now: function () {
        if (Date.now) {
            return Date.now();
        } else {
            return new Date().getTime();
        }
    },

    cap: function (theString) {
        return theString.slice(0, 1).toUpperCase() + theString.slice(1);
    },

    uncap: function (theString) {
        return theString.slice(0, 1).toLowerCase() + theString.slice(1);
    },

    pad: function (theNumber, theLength) {
        if (arguments.length === 1) {
            theLength = 2;
        }

        var prefix = "";
        if (parseInt(theNumber, 10) < 0) {
            prefix = "-";
            theNumber = "" + (-1 * (parseInt(theNumber, 10)));
        } else {
            theNumber = "" + theNumber;
        }

        while (theNumber.length < theLength) {
            theNumber = "0" + theNumber;
        }

        return prefix + theNumber;
    },

    indexOf: function (theArray, theItem) {
        var i, item;
        
        if (koios.exists(theArray)) {
            if (theArray.indexOf) {
                return theArray.indexOf(theItem);
            } else {
                for (i = 0; i < theArray.length; i += 1) {
                    item = theArray[i];
                    if (item === theItem) {
                        return i;
                    }
                }
            }
        }

        return -1;
    },

    argumentsToArray: function (theArgs) {
        return Array.prototype.slice.call(theArgs);
    },

    each: function (theArray, theMethod) {
        if (Array.prototype.hasOwnProperty("forEach")) {
            Array.prototype.forEach.call(theArray, theMethod);
        } else {
            koios.array(theArray).forEach(theMethod);
        }
    },
    
// =================================================
// Templating

    template: function (templateString, replacements, useDeep) {
        if (useDeep) {
            koios.log("deepTemplate: " + templateString);
            
        } else {
            return koios._quickTemplate(templateString, replacements);
        }
    },

    _quickTemplate: function (templateString, replacements) {
        var fn = function (theMacro, theName) {
            var res = theMacro;
            if (replacements.hasOwnProperty(theName)) {
                res = replacements[theName];
            }

            if (res !== theMacro) {
                return res;
            } else {
                return theMacro;
            }
        },
            result = templateString,
            rx = koios.templates[koios.config("templateStyle")];
        
        return result.replace(rx, fn);
    },

    templates: {
        handlebars: /\{{([^{}]*)}}/g,
        "es6": /\$\{([^{}]*)\}/g,
    },
});

/*
KoiosJS: A Cross-Platform Javascript Application Toolkit
www.koiosjs.com
*/

koios.mixin(koios, {
    notFound: -1,

    _arrayWrapper: {
    	_hasEvery: Array.prototype.every,
    	_hasFilter: Array.prototype.filter,
    	_hasForEach: Array.prototype.forEach,
    	_hasIndexOf: Array.prototype.indexOf,
    	_hasLastIndexOf: Array.prototype.lastIndexOf,
    	_hasMap: Array.prototype.map,
    	_hasReduce: Array.prototype.reduce,
    	_hasReduceRight: Array.prototype.reduceRight,
    	_hasSome: Array.prototype.some,

    	all: function(theCallback) {
    		return this.every(theCallback);
        },

    	append: function(theItem) {
            return this.theObject.concat(theItem);
        },

        compact: function() {
            return this.filter(function(theItem) {
                return koios.exists(theItem);
            });
        },

        concat: function(theItem) {
            return this.append(theItem);
        },

        contains: function(theItem) {
            return this.indexOf(theItem) > koios.notFound;
        },

        difference: function(filterOut) {
            if (!koios.isArray(filterOut)) {
                filterOut = [filterOut];
            }

            this.reject(function(theItem) {
                return koios.indexOf(filterOut, theItem) > koios.notFound;
            });
        },

        every: function(theCallback) {
            if (this._hasEvery) {
                return this.theObject.every(theCallback);
            } else {
                for (var i=0;i<this.theObject.length;i++) {
                    var key = this.theObject[i];
                    if (!theCallback(key)) {
                        return false;
                    }
                }
                return true;
            }
        },

        equals: function(theArray) {
            if (this.theObject === theArray) {
                return true;
            }

            if (this.theObject === null || theArray === null) {
                return false;
            }

            if (this.theObject.length !== theArray.length) {
                return false;
            }

            for (var i=0;i<this.theObject.length;i++) {
                if (this.theObject[i] !== theArray[i]) {
                    return false;
                }
            }

            return true;
        },

        forEach: function(theCallback) {
            if (this._hasForEach) {
                this.theObject.forEach(theCallback);
            } else {
                for (var i=0;i<this.theObject.length;i++) {
                    theCallback(this.theObject[i]);
                }
            }
        },

        filter: function(theCallback) {
            if (this._hasFilter) {
                return this.theObject.filter(theCallback);
            } else {
                var ret = [];
                for (var i=0;i<this.theObject.length;i++) {
                    if (theCallback(this.theObject[i])) {
                        ret.push(this.theObject[i]);
                    }
                }

                return ret;
            }
        },

    	indexOf: function(theItem) {
            return koios.indexOf(this.theObject, theItem);
        },

        map: function(theCallback) {
            if (this._hasMap) {
                return this.theObject.map(theCallback);
            } else {
                var ret = [];
                for (var i=0;i<this.theObject.length;i++) {
                    ret.push(theCallback(key));
                }

                return ret;
            }
        },

        none: function(theCallback) {
            for (var i=0;i<this.theObject.length;i++) {
                if (theCallback(this.theObject[i])) {
                    return false;
                }
            }

            return true;
        },

        reduce: function(theCallback) {
            if (this._hasReduce) {
                return this.theObject.reduce(theCallback);
            } else {
                if (this.theObject.length === 0) {
                    return this.theObject[0];
                }

                var ret = this.theObject[0];
                for (var i=1;i<this.theObject.length;i++) {
                    ret = theCallback(ret, this.theObject[i], i, this.theObject);
                }

                return ret;
            }
        },


    	reduceLeft: function(theCallback) {
            return this.reduce(theCallback);
        },

        reduceRight: function(theCallback) {
            if (this._hasReduceRight) {
                return this.theObject.reduceRight(theCallback);
            }

            if (this.theObject.length === 0) {
                return this.theObject[0];
            }

            var ret = this.theObject[theObject.length-1];
            for (var i=this.theObject.length-1; i > -1; i--) {
                ret = theCallback(ret, this.theObject[i], i, this.theObject);
            }
            return ret;
        },

        reject: function(theCallback) {
            var ret = [];
            for (var i=0;i<this.theObject.length;i++) {
                if (!theCallback(this.theObject[i])) {
                    ret.push(this.theObject[i]);
                }
            }

            return ret;
        },

        remove: function(theItem) {
            var ind = this.indexOf(theItem);
            if (ind > koios.notFound) {
                this.theObject.splice(ind, 1);
            }

            return this.theObject;
        },

        shuffle: function() {
            var obj = koios.clone(this.theObject);
            obj.sort(function() {
                return Math.random() - 0.5;
            });
        },

        some: function(theCallback) {
            if (this._hasSome) {
                return this.theObject.some(theCallback);
            }

            for (var i=0;i<this.theObject.length;i++) {
                if (theCallback(this.theObject[i])) {
                    return true;
                }
            }

            return false;
        },

        toArray: function() {
            return this.theObject;
        },

        toString: function() {
            return koios.JSON.stringify(this.theObject);
        },
    },

    array: function(theArray) {
        return new koios.wrapper(theArray, koios._arrayWrapper);
    }
});

/*
KoiosJS: A Cross-Platform Javascript Application Toolkit
www.koiosjs.com
*/

koios.mixin(koios, {
    _objectWrapper: {
        contains: function(theKey) {
            return theKey in this.theObject;
        },

        equals: function(otherObject) {
            if (this.theObject === otherObject) {
                return true;
            }

            if (!this.theObject || !otherObject) {
                return false;
            }

            if (this.keys().length !== koios.object(otherObject).keys().length) {
                return false;
            }

            for (key in this.theObject) {
                if (!koios.hasProp(this.theObject, key)) {
                    if (this.theObject[key] !== otherObject[key]) {
                        return false;
                    }
                }
            }

            return true;
        },

        filter: function(theCallback) {
            var ret = {};
            this.iterate(function(key, value, idx, obj) {
                if (theCallback(key, value, idx, obj)) {
                    ret[key] = value;
                }
            });

            return ret;
        },

        forEach: function(theCallback) {
            return this.iterate(theCallback);
        },

        functions: function(asFunctions) {
            if (asFunctions) {
                return koios.array(this.values()).filter(function(theItem) {
                    return koios.isFunction(theItem);
                });
            } else {
                return koios.object(this.filter(function(key, value) {
                    return koios.isFunction(value);
                })).keys();
            }
        },

        invert: function() {
            var obj = {};
            this.iterate(function(key, value) {
                obj[value] = key;
            });

            return obj;
        },

        iterate: function(theCallback) {
            var keys = this.keys();
            for (var i=0;i<keys.length;i++) {
                var key = keys[i];
                theCallback(key, this.theObject[key], i, this.theObject);
            }
        },

        keys: function() {
            var keys = [];
            for (var key in this.theObject) {
                if (!koios.hasProp(this.theObject, key)) {
                    keys.push(key);
                }
            }

            return keys;
        },

        length: function() {
            return this.keys().length;
        },

        map: function(theCallback) {
            var ret = [];
            this.iterate(function(key, value, idx, obj) {
                ret.push(theCallback(key,value,idx,obj));
            });
        },

        mixin: function(theSource) {
            var theObject = koios.clone(this.theObject);
            return koios.mixin(theObject, theSource);
        },

        pairs: function() {
            var ret = [];
            this.iterate(function(key, value, idx, obj) {
                ret.push([key,value]);
            });

            return ret;
        },

        reject: function(theCallback) {
            var ret = {};
            this.iterate(function(key, value, idx, obj) {
                if (!theCallback(key, value, idx, obj)) {
                    ret[key] = value;
                }
            });
        },

        toObject: function() {
            return this.theObject;
        },

        toString: function() {
            return koios.JSON.stringify(this.theObject);
        },

        values: function() {
            var ret = [];
            this.iterate(function(key, value) {
                ret.push(value);
            });

            return ret;
        },
    },

    object: function(theObject) {
        return new koios.wrapper(theObject, koios._objectWrapper);
    },
});

/*
KoiosJS: A Cross-Platform Javascript Application Toolkit
www.koiosjs.com
*/

koios.mixin(koios, {
    _stringWrapper: {
        escape: function() {
            if (this.theObject !== null) {
                return String(this.theObject).replace(/&/g,'&amp;').replace(/</g,'&lt;').replace(/>/g,'&gt;');
            } else {
                return "";
            }
        },

        camelCase: function(divider) {
            if (!divider) {
                divider = "-";
            }
            
            var s = this.trimWhitespace();
            if (s.indexOf(divider) === -1) {
                return s;
            }

            var first = true;
            var p = koios.array(s.split(divider)).map(function(theItem) {
                if (!first) {
                    return koios.cap(theItem);
                } else {
                    first = false;
                    return theItem;
                }
            });

            return p.join("");
        },

        contains: function(theSubstring, ignoreCase) {
            if (ignoreCase) {
                return this.theObject.toLowerCase().indexOf(theSubstring.toLowerCase()) > -1;
            } else {
                return this.theObject.indexOf(theSubstring) > -1;
            }
        },

        endsWith: function(theEnding, ignoreCase) {
            var len = theEnding.length;
            var endStr = this.theObject.substr(this.theObject.length-len);
            if (ignoreCase) {
                return endStr.toLowerCase() === theEnding.toLowerCase();
            } else {
                return endStr === theEnding;
            }
        },

        repeat: function(theNumber) {
            var str = koios.clone(this.theObject);
            var len = str.length * theNumber;
            while(str.length < len) {
                str += this.theObject;
            }

            return str;
        },

        startsWith: function(theStarting, ignoreCase) {
            var len = theStarting.length;
            var startStr = this.theObject.substr(0, len);
            if (ignoreCase) {
                return startStr.toLowerCase() === theStarting.toLowerCase();
            } else {
                return startStr === theStarting;
            }
        },

        toCharArray: function() {
            return this.theObject.split("");
        },

        toString: function() {
            return this.theObject;
        },

        trimWhitespace: function() {
            LTrim = function(value) {
                var re = /\s*((\S+\s*)*)/;
                return value.replace(re, "$1");
            }
            RTrim = function(value) {
                var re = /((\s*\S+)*)\s*/;
                return value.replace(re, "$1");
            }
            return LTrim(RTrim(this.theObject));
        },
    },

    string: function(theString) {
        return new koios.wrapper(theString, koios._stringWrapper);
    }
});

/*
KoiosJS: A Cross-Platform Javascript Application Toolkit
www.koiosjs.com
*/

koios.wrapper = function(theObj, mixin) {
    var obj = {theObject:theObj};
    koios.mixin(obj, mixin);
    return obj;
}

/*
KoiosJS: A Cross-Platform Javascript Application Toolkit
www.koiosjs.com
*/

koios.component = function() {};

koios.mixin(koios, {
    _id: 0,
    _uid: function() {
        return koios._id++;
    },

    getConstructor: function(className) {
        if (koios.isFunction(className) || !className) {
            return className;
        }

        var parts = className.split(".");
        var obj = global;
        for (var i=0;i<parts.length;i++) {
            var key = parts[i];
            obj = obj[key];
            if (!obj) {
                return {};
            }
        }

        return obj;
    },

    _makeConstructor: function() {
        return function() {
            if (this._ct) {
                this._ct.apply(this, arguments);
            }
            if (this.init) {
                this.init.apply(this, arguments);
            }

            return this;
        }
    },

    _setPrototype: function(name, ctor, proto) {
		koios.component.prototype = proto;
		ctor.prototype = new koios.component();
    },

    _setObject: function(className, value) {
        var parts = className.split(".");
        var obj = global;

        for (var i=0;i<parts.length-1;i++) {
            var key = parts[i];
            if (!obj[key]) {
                obj[key] = {};
            }
            obj = obj[key];
        }

        obj[parts[parts.length-1]] = value;
    },

    arguments: function(theArgs) {
        if (!theArgs) {
            return [];
        }

        var args = koios.argumentsToArray(theArgs);
        var lastArg = args[args.length-1];
        if (koios.isObject(lastArg) && lastArg._sender) {
            args.splice(args.length-1);
        }

        return args;
    }
});

/*
KoiosJS: A Cross-Platform Javascript Application Toolkit
www.koiosjs.com
*/

koios.define = function(theObject) {
    // Make sure we're defining something
    if (!theObject || !koios.isObject(theObject)) {
        return koios.throw("koios.define() error: You must pass an object as the first parameter of koios.define()");
    }

    var name = theObject.className;
    if (!name) {
        return koios.throw("koios.define() error: You must give your new class a name.");
    }

	if (theObject.hasOwnProperty("constructor")) {
		theObject._ct = theObject.constructor;
		delete theObject.constructor;
	}

    var newCtor = koios._makeConstructor();
    var extClass = theObject.extend;
    var extend = extClass && koios.getConstructor(extClass);
    var proto = extend && extend.prototype;
    if (theObject.extend && !proto) {
        // throw "koios.define() error: Attempting to subclass a class '"+extClass+"' which does not exist";
        return koios.throw("koios.define() error: Attempting to subclass a class '"+extClass+"' which does not exist");
    }

    delete theObject.extend;

    if (proto) {
        koios._setPrototype(name, newCtor, proto);
    } else {
        koios._setPrototype(name, newCtor, {});
    }

    newCtor.prototype.className = name;
    newCtor.prototype._extends = extend;
    newCtor.prototype._ctor = newCtor;

    koios.mixin(newCtor.prototype, theObject);

    // koios.define() plugins
    for (var i=0;i<koios.define.mixins.length;i++) {
        koios.define.mixins[i](name, newCtor, newCtor.prototype);
    }

    koios._setObject(name, newCtor);

    if (!theObject.ignoreAlias) {
        var alias = ("alias" in theObject) ? theObject.alias : name.toLowerCase();
        if (alias !== name) {
            koios._setObject(alias, function(args) {
                return new newCtor(args);
            });
        }
    }

    return newCtor;
};

koios.define.mixins = [];
koios.define.addMixin = function(theMixin) {
	koios.define.mixins.push(theMixin);
};

/*
KoiosJS: A Cross-Platform Javascript Application Toolkit
www.koiosjs.com
*/

// =================================================
// Core OO Statics

koios.define.addMixin(function(theName, theConstructor, thePrototype) {
    theConstructor.create = function(theObject) {
        return new this(theObject);
    };

    theConstructor.extend = function(theObject) {
        theObject.extend = thePrototype.className;
        koios.define(theObject);
    };

    theConstructor.mixin = function(theObject) {
        koios.mixin(this.prototype, theObject);
    };
});

// =================================================
// Additional Statics Support

koios.define.addMixin(function(theName, theConstructor, thePrototype) {
    if (thePrototype.statics) {
        koios.mixin(theConstructor, thePrototype.statics);
        delete thePrototype.statics;
    }
});

// =================================================
// Accumulated Properties

_addAccumulated = function(accumulated, extended) {
    if (koios.indexOf(accumulated, extended) === -1) {
        accumulated.push(extended);
    }
};

koios.define.addMixin(function(theName, theConstructor, thePrototype) {
    if (!thePrototype.accumulated) {
        thePrototype.accumulated = [];
    }

    if (thePrototype._extends) {
        var eAcc = thePrototype._extends.prototype.accumulated;
        for (var i=0;i<eAcc.length;i++) {
            var extendAcc = eAcc[i];
            _addAccumulated(thePrototype.accumulated, extendAcc);
        }
    }

    for (var i=0;i<thePrototype.accumulated.length;i++) {
        if (thePrototype._extends) {
            var key = thePrototype.accumulated[i];
            var val = koios.clone(thePrototype._extends.prototype[key]);
            var curVal = thePrototype[key];
            var newVal;
            if (koios.isObject(val)) {
                curVal = koios.mixin({}, thePrototype[key]);
                newVal = koios.mixin({}, val);
                thePrototype[key] = koios.mixin(newVal, curVal);
            } else if (koios.isArray(val)) {
                for (var n=0;n<val.length;n++) {
                    if (koios.indexOf(curVal, val[n]) === -1) {
                        thePrototype[key].push(val[n]);
                    }
                }
            } else {
                thePrototype[key] = curVal;
            }
        }
    }
});

// =================================================
// Inherit Support

koios.define.addMixin(function(theName, theConstructor, thePrototype) {
    if (thePrototype.inherit) {
        var inherit = thePrototype.inherit;
        if (!koios.isArray(inherit)) {
            inherit = [inherit];
        }

        koios.each(inherit, function(theItem) {
            var ctor = koios.getConstructor(theItem);
            var proto = ctor.prototype;
            if (koios.isaSubclassOf(proto, "koios.Mixin")) {
                proto.mixin(theConstructor, thePrototype);
                koios.mixin(thePrototype, proto.mixinProperties);
            }
        });

        delete thePrototype.inherit;
    }
});

// =================================================
// _super support

koios.define.addMixin(function(theName, theConstructor, thePrototype) {
    var fns = koios.object(thePrototype).functions(true);
    koios.each(fns, function(theItem) {
        if (!theItem._owner) {
            theItem._owner = theName;
        }
    });
});


/*
KoiosJS: A Cross-Platform Javascript Application Toolkit
www.koiosjs.com
*/

koios.singleton = function(theObject, theContext) {
    var name = theObject.alias || theObject.className;
    theObject.ignoreAlias = true;

    var obj = koios.define(theObject);
    koios._setObject(name, new obj(), theContext);
};

/*
KoiosJS: A Cross-Platform Javascript Application Toolkit
www.koiosjs.com
*/

koios._super = function(thePrototype, theContext, theMethod, theArguments) {
    var args = [];
    if (koios.isArray(theArguments)) {
        args = theArguments.slice();
    } else if (theArguments) {
        args = koios.argumentsToArray(theArguments);
    }

    var proto = null;
    for (var i=0;i<args.length;i++) {
        var key = args[i];
        if (koios.isObject(key) && key._sender) {
            proto = key._sender;
            args.splice(i);
        }
    }

    var className, extend;
    if (proto) {
        className = proto.className;
		extend = proto._extends.prototype;
    } else {
        while (thePrototype._extends) {
            if (thePrototype.className === thePrototype[theMethod]._owner) {
                break;
            }
            thePrototype = thePrototype._extends.prototype;
        }

        className = thePrototype.className;
        extend = thePrototype._extends.prototype;
    }

    if (!extend) {
        // For testing...
        koios.throw("koios._super serious error: extend doesn't exist.");
    }

    if (extend.className) {
        var nextClass;
        if (extend[theMethod] && extend[theMethod]._owner === extend.className) {
            nextClass = extend.className;
        } else {
            nextClass = extend[theMethod]._owner;
        }

        var nextProto = koios.getConstructor(nextClass).prototype;
        args.push({_sender:nextProto});
        nextProto[theMethod].apply(theContext, args);
    }
};

_makeSuper = function(thePrototype) {
    return function(method, args) {
        koios._super(thePrototype, this, method, args);
    };
};

koios.define.addMixin(function(theName, theConstructor, thePrototype) {
    thePrototype._super = _makeSuper(thePrototype);
});

/*
KoiosJS: A Cross-Platform Javascript Application Toolkit
www.koiosjs.com
*/

koios.define({
    className: "koios.Class",
    ignoreAlias: true,
    _bindable: true,
    _destroyed: false,
    _constructor: "_ct",
    accumulated: [],

    constructor: function() {
        this.__id = koios._uid();
        koios.mixin(this, koios.arguments(arguments)[0]);
    },

    init: function() {
        if (!this.name) {
            this._generateObjectName();
        }

        var fns = koios.object(this._ctor.prototype).functions(true);
        var t = this;
        koios.each(fns, function(theItem) {
            if (theItem._init) {
                theItem.call(t);
            }
        });
    },

    _generateObjectName: function() {
        className = this.className || this.prototype.className;
        tempName = className+"_"+this.__id;
        this.name = tempName.replace(".", "");
    },

// =================================================
// Function Handling

    call: function() {
        var fnName = arguments[0];
        var args = Array.prototype.splice.call(arguments, 1);

        if (koios.isFunction(fnName)) {
            fnName.apply(this, args);
        } else {
            if (!this[fnName]) {
                if (koios.config("warnEmptyCalls")) {
                    koios.warn(this.className+".call() warning: method "+fnName+" does not exist");
                }
            } else {
                this[fnName].apply(this, args);
            }
        }
    },

    bind: function(fnName) {
        if (!this._destroyed && this._bindable) {
            return koios.bind(this, fnName);
        } else {
            return null;
        }
    },

// =================================================
// Subclass Checking

    isa: function(className) {
        if (koios.isString(className)) {
            return this.className.toLowerCase() === className.toLowerCase();
        } else {
            try {
                return this.className.toLowerCase() === className.className.toLowerCase();
            } catch(error) {
                return false;
            }
        }
    },

    isaSubclassOf: function(className) {
        if (!koios.isString(className)) {
            className = className.className;
        }

        className = className.toLowerCase();
        var proto = this._ctor.prototype;
        while (proto) {
            if (proto.className.toLowerCase() === className) {
                return true;
            }

            if (proto._extends) {
                proto = proto._extends.prototype;
            } else {
                proto = null;
            }
        }

        return false;
    },

// =================================================
// Garbage Collection

    destroy: function() {
		this._destroyed = true;
        this._bindable = false;
    },


});

/*
KoiosJS: A Cross-Platform Javascript Application Toolkit
www.koiosjs.com
*/

koios.define({
    className:"koios.Mixin",
    extend:"koios.Class",
    ignoreAlias: true,

    mixin: function(theConstructor, thePrototype) {

    },

    mixinProperties:{},
});

/*
KoiosJS: A Cross-Platform Javascript Application Toolkit
www.koiosjs.com
*/

// koios.define({
//     className:"koios.Strategy",
//     ignoreAlias: true,

//     value: function() {
//         koios.warn(this.className+"value() warning - koios.Strategy base value() method not overriden, no value returned.");
//     }
// });

// koios.mixin(koios, {
// 	_strategyMap: {},
// 	strategy: function(name) {
// 		if (koios._strategyMap[name]) {
// 			return koios._strategyMap[name];
// 		} else {
// 			var ctor = koios.getConstructor(name);
// 			return koios._strategyMap[name] = new ctor();
// 		}
// 	}
// });

koios.strategy = {
	device: (typeof window !== "undefined") ? "window" : "headless",
	
};
/*
KoiosJS: A Cross-Platform Javascript Application Toolkit
www.koiosjs.com
*/

koios.define({
	className:"koios.CloneSupport",
	extend: "koios.Mixin",

	mixin: function(theConstructor, thePrototype) {

	},

	mixinProperties: {
		_clonable: [],
		clone: function() {
			var obj = {}, t = this;
			koios.each(this._clonable, function(theItem) {
				obj[theItem] = koios.clone(t[theItem]);
			});

			var className = obj.className || this.className;
			var ctor = koios.getConstructor(className);
			if (!koios.isEmpty(ctor)) {
				return new ctor(obj);
			}
		}
	}
})
/*
KoiosJS: A Cross-Platform Javascript Application Toolkit
www.koiosjs.com
*/

koios.define({
    className:"koios.EventSupport",
    extend: "koios.Mixin",
    ignoreAlias: true,

    mixin: function(theConstructor, thePrototype) {
        if (koios.indexOf(thePrototype.accumulated, "events") === -1) {
            thePrototype.accumulated.push("events");
        }

        thePrototype._listeners = [];
    },

    mixinProperties: {
        emit: function(namespace, eventName) {
            if (namespace.indexOf("::") === -1) {
                koios.emit.apply(koios, [this.name].concat(Array.prototype.slice.call(arguments)));
            } else {
                koios.emit.apply(koios, Array.prototype.slice.call(arguments));                
            }
        },

        handlers: function(filter) {
            return koios.array(this._listeners).filter(function(theItem) {
                if (!filter)
                    return true;
                if (theItem.handles && theItem.handles === filter)
                    return true;
                return false;
            });
        },

        off: function(theHandler) {
            if (theHandler.handles) {
                this._listeners = koios.array(this._listeners).remove(theHandler);
                koios.MessageEmitter.off(theHandler.handles, theHandler);
            } else if (koios.isString(theHandler)) {
                var events = this.handlers(theHandler);
                for (var i=0;i<events.length;i++) {
                    this._listeners = koios.array(this._listeners).remove(events[i]);
                    koios.MessageEmitter.off(events[i].handles, events[i]);
                }
            } else if (arguments.length === 0) {
                this.removeAllListeners();
            }
        },

        on: function(eventName, theHandler) {
            if (this._destroyed) {
                return;
            }
            
            if (eventName.indexOf("::") === -1) {
                eventName = this.name+"::"+eventName;
            }

            theHandler = {
                owner:this,
                count:-1,
                listener:theHandler,
                handles: eventName
            };

            var listener = koios.MessageEmitter.addListener(eventName, theHandler);
            this._listeners.push(listener);
            return listener;
        },

        removeAllListeners: function() {
            var events = this.handlers();
            for (var i=0;i<events.length;i++) {
                this._listeners = koios.array(this._listeners).remove(events[i]);
                koios.MessageEmitter.off(events[i].handles, events[i]);
            }
            this._listeners.length = 0;
        }
    }
});

/*
KoiosJS: A Cross-Platform Javascript Application Toolkit
www.koiosjs.com
*/

koios.define({
    className:"koios.JSONSupport",
    extend:"koios.Mixin",
    ignoreAlias: true,

    mixinProperties: {
        toObject: function() {
            var _stringable = this._stringable;
            if (koios.isFunction(_stringable)) {
                _stringable = this._stringable();
            }

            var ret = {};
            for (var i=0;i<_stringable.length;i++) {
                var key = _stringable[i];
                if (key in this) {
                    ret[key] = this[key];
                } else if (this.get) {
                    ret[key] = this.get(key);
                } else {
                    ret[key] = null;
                }
            }
            return ret;
        },

        toString: function() {
            return koios.JSON.stringify(this.toObject());
        },

        log: function() {
            koios.log(this.toString());
        }
    },
})

/*
KoiosJS: A Cross-Platform Javascript Application Toolkit
www.koiosjs.com
*/

// =================================================
// Decorators

decorateProp = function(propVal) {
    var val = koios.clone(propVal);
    val.default = propVal.value;
    if (!val.get) {
        val.get = function() {
            return val.value;
        }
    }

    if (!val.set) {
        val.set = function(value) {
            val.value = value;
        }
    }

    return val;
}

_validateProp = function(prop, propVal, propType) {
	var shouldUpdate = true;
	if (prop.validate && prop.validate[propType]) {
        if (koios.isString(prop.validate[propType])) {
    		shouldUpdate = prop.owner.call(prop.validate[propType], propVal, prop, prop.owner);
        } else {
            shouldUpdate = prop.validate[propType](propVal, prop, prop.owner);
        }
    }

	return shouldUpdate;
}

// =================================================

koios.define({
    className: "koios.PropertySupport",
    extend: "koios.Mixin",
    ignoreAlias: true,

    mixin: function(theConstructor, thePrototype) {
        if (!thePrototype.properties) {
            thePrototype.properties = {};
        }

        if (koios.indexOf(thePrototype.accumulated, "properties") === -1) {
            thePrototype.accumulated.push("properties");
        }

        this.mixinProperties._setProps._init = true;
    },

    mixinProperties: {

// =================================================
// Define the property wrappers...

        _setProps: function() {
            this._props = {};
            for (var key in this.properties) {
                var value = this.properties[key];
                this.addProperty(key, value);
            }
        },

// =================================================
// Runtime property support

		addProperty: function(propName, propVal) {
            if (arguments.length === 1 || !propVal) {
                propVal = {};
            }

            var val = decorateProp(propVal);
            val.name = propName;
            val.owner = this;
            this._props[propName] = val;
        },

        allProperties: function() {
            return koios.object(this._props).keys();
        },

// =================================================
// Property Value manipulation

        add: function(propName, propItem) {
            var prop = this._props[propName];
            if (!prop) {
                return null;
            }

            if (!_validateProp(prop, propItem, "add")) {
                return;
            }

            var val = koios.clone(prop.value);
            if (koios.isArray(val)) {
                val.push(propItem);
            }

            this.set(propName, val);
        },

        freeze: function(propName) {
            if (this._props[propName]) {
                this._props[propName].frozen = true;
            }
        },

        get: function(propName) {
            if (arguments.length === 0) {
                return this.getAll();
            } else {
                return (this._props[propName]) ? this._props[propName].value : null;
            }
        },

        getAll: function() {
            var obj = {};
            koios.object(this._props).forEach(function(key, value) {
                obj[key] = value.get(key);
            });

            return obj;
        },

        remove: function(propName, propItem) {
            var prop = this._props[propName];
            if (!prop) {
                return null;
            }

            if (!_validateProp(prop, propItem, "remove")) {
                return;
            }

            var val = koios.clone(prop.value);
            if (koios.isArray(val)) {
                val = koios.array(val).remove(propItem);
            }

            this.set(propName, val);
        },

        reset: function(propName) {
            var prop = this._props[propName];
            if (!prop)
                return;

            this.set(propName, prop.default);
        },

        set: function(propName, propVal) {
            var prop = this._props[propName];
            if (!prop) {
                return;
            }

            if (prop.frozen) {
                if (koios.config("warnFrozenProperties")) {
                    koios.warn(this.className+".set("+propName+") warning: property frozen.");
                }
                return;
            }

            this.emit(propName+":beforeChange");

            if (!_validateProp(prop, propVal, "set")) {
                console.log("not set.");
                return;
            }

            koios.log("setting "+this.className+"."+propName+" to "+propVal, koios.Logger.INTERNAL);
            prop.value = propVal;
            this.emit(propName+":change");
        },

        unfreeze: function(propName) {
            if (this._props[propName]) {
                this._props[propName].frozen = false;
            }
        },
    }
});

/*
KoiosJS: A Cross-Platform Javascript Application Toolkit
www.koiosjs.com
*/

koios.define({
    className: "koios.TaskSupport",
    extend: "koios.Mixin",
    ignoreAlias: true,

    mixin: function(theConstructor, thePrototype) {
        if (!thePrototype.tasks) {
            thePrototype.tasks = [];
        }

        this.mixinProperties._setTasks._init = true;
    },

    mixinProperties: {
        _setTasks: function() {
            var _tasks = [];
            for (var i=0;i<this.tasks.length;i++) {
                var _task = this.tasks[i];
                _task.owner = this;
                var newTask = new koios.Task(this.tasks[i]);
                _tasks.push(newTask);
            }

            this._tasks = _tasks;
        },

        _taskParams: function(task, args) {
            var firstArg = args[0];
            if (task.params && koios.isArray(task.params) && koios.isObject(firstArg)) {
                var _args = [];
                koios.each(task.params, function(theParam) {
                    if (koios.isString(theParam)) {
                        _args.push(koios.template(theParam, firstArg));
                    } else {
                        _args.push(theParam);
                    }
                });
                _args = _args.concat(Array.prototype.slice.call(args, 1));
                return _args;
            } else {
                return args;
            }
        },

        getTask: function(taskName) {
            if (koios.isString(taskName)) {
                var tasks = koios.array(this._tasks).filter(function(theTask) {
                    return (theTask.name === taskName);
                });
                if (tasks.length > 0) {
                    var task = tasks[0];

                    return task.clone();
                } else {
                    return null;
                }
            } else if (koios.isFunction(taskName)) {
                var task = new koios.Task({method:taskName});
                task.owner = this;
                return task;
            } else if (koios.isaSubclassOf(taskName, "koios.Task")) {
                taskName.owner = this;
                return taskName;
            }
        },

        task: function() {
            var taskName = arguments[0];
            var task = this.getTask(taskName);
            if (task) {
                var args = Array.prototype.slice.call(arguments, 1);
                var args = this._taskParams(task, args);
                task.enqueue.apply(task, args);
            }
            return task;
        }
    }
});

/*
KoiosJS: A Cross-Platform Javascript Application Toolkit
www.koiosjs.com
*/

koios.singleton({
    className:"koios.EventHandler",
    extend:"koios.Class",
    ignoreAlias: true,

    listen: function(eventName, eventHandler, scope) {
        if (!scope) {
            scope = (typeof window !== "undefined") ? window : this;
        }
        
        if (typeof window !== "undefined") {
            scope.addEventListener(eventName, eventHandler, false);
        }
    },

});

/*
KoiosJS: A Cross-Platform Javascript Application Toolkit
www.koiosjs.com
*/

/*
KoiosJS: A Cross-Platform Javascript Application Toolkit
www.koiosjs.com
*/

/*jslint sloppy:true*/
/*jslint nomen:true*/
/*global koios*/

koios.singleton({
    className: "koios.MessageEmitter",
    extend: "koios.Class",
    ignoreAlias: true,

    init: function () {
        this._super("init", arguments);

        this.events = {};
        this._broadcastable = [];
    },

// =================================================
// Listener Retrieval

    getListeners: function (theEvent) {
        if (koios.isRegex(theEvent)) {
            var ret = {},
                key;
            
            for (key in this.events) {
                if (this.events.hasOwnProperty(key) && theEvent.test(key)) {
                    ret[key] = this.events[key];
                }
            }

            return ret;
        } else {
            if (this.events[theEvent]) {
                return this.events[theEvent];
            }
            
            this.events[theEvent] = [];
            return [];
        }
    },

    _listenersAsObject: function (theEvent) {
        var listeners = this.getListeners(theEvent),
            obj = {};
        
        if (koios.isArray(listeners)) {
            obj[theEvent] = listeners;
            return obj;
        } else {
            return obj;
        }
    },

// =================================================
// Broadcasting

    broadcast: function (eventName) {
        if (!koios.isArray(eventName)) {
            eventName = [eventName];
        }

        var t = this;
        koios.each(eventName, function (theItem) {
            t.getListeners(theItem);
            t._broadcastable.push(theItem);
        });
    },

    messages: function (eventName) {
        return this._broadcastable;
    },

    unbroadcast: function (theEvent) {
        var key;
        if (koios.isString(theEvent)) {
            delete this.events[theEvent];
        } else if (koios.isRegex(theEvent)) {
            for (key in this.events) {
                if (this.events.hasOwnProperty(key) && theEvent.test(key)) {
                    delete this.events[key];
                }
            }
        } else if (arguments.length === 0) {
            this.events = {};
        }

        return this;
    },

// =================================================
// Listener Manipulation

    addListener: function (theEvent, theListener) {
        var listeners = this._listenersAsObject(theEvent),
            key;
        
        if (koios.isFunction(theListener)) {
            theListener = {
                count: -1,
                listener: theListener
            };
        }

        if (!theListener) {
            return;
        }

        if (!theListener.owner) {
            theListener.owner = this;
        }

        for (key in listeners) {
            if (listeners.hasOwnProperty(key) && koios.indexOf(listeners[key], theListener) === -1) {
                this.events[key].push(theListener);
            }
        }

        return theListener;
    },

    off: function (theEvent, theListener) {
        this.removeListener(theEvent, theListener);
    },

    on: function (theEvent, theListener) {
        this.addListener(theEvent, theListener);
    },

    once: function (theEvent, theListener) {
        if (koios.isFunction(theListener)) {
            theListener = {
                count: 1,
                listener: theListener
            };
        }

        return this.addListener(theEvent, theListener);
    },

    removeListener: function (theEvent, theListener) {
        var listeners = this._listenersAsObject(theEvent),
            key,
            idx;
        
        for (key in listeners) {
            if (listeners.hasOwnProperty(key)) {
                idx = koios.indexOf(listeners[key], theListener);
                if (idx !== -1) {
                    listeners[key].splice(idx, 1);
                }

                if (listeners[key].length === 0) {
                    delete this.events[key];
                }
            }
        }

        return this;
    },

// =================================================
// Event Emitting

    emit: function (theEvent) {
        var args = Array.prototype.slice.call(arguments, 1),
            listeners = this._listenersAsObject(theEvent),
            i,
            key,
            evtListeners,
            t = this;
        
        if (!args) {
            args = [];
        }
                
        for (key in listeners) {
            if (listeners.hasOwnProperty(key)) {
                evtListeners = listeners[key];
                koios.each(evtListeners, function (theListener) {
                    theListener.listener.apply(theListener.owner, args);
                    if ((theListener.hasOwnProperty("count")) && theListener.count > -1) {
                        theListener.count -= 1;
                        if (theListener.count === 0) {
                            t.removeListener(theEvent, theListener);
                        }
                    }
                });
            }
        }
    }
});

koios.on = koios.bind(koios.MessageEmitter, "on");
//};

koios.off =  koios.bind(koios.MessageEmitter, "off");
koios.emit = function (namespace, messageName) {
    var evName = namespace,
        ct = 1,
        args;
    
    if (namespace.indexOf(":") === -1) {
        evName += "::" + messageName;
        ct += 1;
    }
    console.log("emitting: " + evName);
    args = [evName].concat(Array.prototype.slice.call(arguments, ct));
    
    koios.MessageEmitter.emit.apply(koios.MessageEmitter, args);
};
/*
KoiosJS: A Cross-Platform Javascript Application Toolkit
www.koiosjs.com
*/

koios.define({
    className:"koios.Logger",
    extend: "koios.Class",
    inherit: ["koios.EventSupport"],
    ignoreAlias: true,

    _logging: true,

    statics: {
		_PRIVATE: 7,
		INTERNAL: 6,
		LOG: 5,
		WARN: 4,
		ERROR: 3,
		ALERT: 2,
		ALL: 1
    },

    _calls: ["alert", "error", "warn", "log", "internal", "_private"],

    init: function() {
        this._super("init", arguments);
        for (var i=0;i<this._calls.length;i++) {
            if (!this[this._calls[i]]) {
                this[this._calls[i]] = function() {};
            }
        }

        if (this._logging) {
            koios.on("koios:log", this.bind("_message"));
        }
    },

    _message: function(theLog, theLevel) {
        if (theLevel === 1) {
            this.call("alert");
        } else {
            this.call(this._calls[theLevel-1], theLog);
        }
    },

    stopLogging: function() {
        if (this._logging) {
            this._logging = false;
            this.off("koios:log");
        }
    },

    startLogging: function() {
        if (!this._logging) {
            this._logging = true;
            koios.on("koios:log", this.bind("_message"));
        }
    }
});

/*
KoiosJS: A Cross-Platform Javascript Application Toolkit
www.koiosjs.com
*/

koios.define({
    className:"koios.ConsoleLogger",
    extend:"koios.Logger",
    ignoreAlias: true,

    init: function() {
        this._super("init", arguments);

        this.console = console;
    },

	alert: function(theLog) {
		this.console.error(theLog);
		if (typeof window !== "undefined" && koios.config("showWindowAlerts")) {
			window.alert(theLog);
        }
    },

	error: function(theLog) {
		this.console.error(theLog);
    },

	warn: function(theLog) {
		this.console.warn(theLog);
    },

	log: function(theLog) {
		this.console.log(theLog);
    },

	internal: function(theLog) {
		this.console.log(theLog);
    },

	_private: function(theLog) {
		this.console.log(theLog);
    }
});

koios._consoleLogger = new koios.ConsoleLogger();

/*
KoiosJS: A Cross-Platform Javascript Application Toolkit
www.koiosjs.com
*/

/*jslint sloppy:true*/
/*jslint nomen:true*/
/*global koios*/

koios.mixin(koios, {
    log: function (theLog, theLevel) {
        if (arguments.length === 1) {
            theLevel = koios.Logger.LOG;
        }
        
        var logLevel = koios.config("logLevel");
        if (theLevel <= logLevel) {
            koios.emit("koios:log", theLog, theLevel);
        }
    },

    warn: function (theLog) {
        koios.log(theLog, koios.Logger.WARN);
    },

    "throw": function (theLog) {
        koios.log(theLog, koios.Logger.ALERT);
        throw theLog;
    }
});

koios.MessageEmitter.broadcast("koios:log");

/*
KoiosJS: A Cross-Platform Javascript Application Toolkit
www.koiosjs.com
*/

koios._promiseResolver = {
    executeCallback: function(callback, promise, args) {
        try {
            result = callback(args);
        } catch(error) {
            return promise.reject(error);
        }

        this.resolve(promise, result);
    },

    resolve: function(promise,result) {
        if (!koios.isObject(result) && !koios.isFunction(result)) {
            promise.fulfill(result);
            return promise;
        }

        if (result === promise) {
            return koios.throw("koios.Promise error(): you cannot fulfill a promise with itself.");
        }

        var _then = null;
        try {
            _then = result.then;
        } catch(error) {
            return promise.reject(error);
        }

        if (!koios.isFunction(then)) {
            return promise.fulfill(result);
        }

        called = false;
        try {
            _then.call(result, function(y) {
                if (called) {
                    return;
                }
                koios._promiseResolve.resolve(promise, y);
                called = true;
            }, function(z) {
                if (called) {
                    return;
                }
                promise.reject(z);
                called = true;
            });
        } catch(error) {
            if (!called) {
                promise.reject(error);
            }
            called = true;
        }
    }
};

// =================================================

koios.define({
    className:"koios.Promise",
    extend:"koios.Class",
	statics: {
		state: {
			REJECTED: -1,
			PENDING: 0,
			FULFILLED: 1
        }
    },

// =================================================
// Setup

    init: function() {
        this._super("init", arguments);

        this.state = koios.Promise.state.PENDING;
        this.value = null;
        this.reason = null;
        this._callbacks = [];
    },

// =================================================
// State

    settled: function() {
        return (this.state !== koios.Promise.state.PENDING);
    },

    isPromise: function(obj) {
        return (koios.isObject(obj) && theObject.then);
    },

// =================================================
// Wrapper

    _callback: function(onFulfilled, onRejected, promise) {
        this._callbacks.push({
            onFulfilled:onFulfilled,
            onRejected:onRejected,
            promise:promise,
        });
    },

// =================================================
// Private Resolution

    _fulfill: function(callback) {
        if (koios.isFunction(callback.onFulfilled)) {
            koios._promiseResolver.executeCallback(callback.onFulfilled, callback.promise, this.value);
        } else {
            callback.promise.fufill(this.value);
        }
    },

    _reject: function(callback) {
        if (koios.isFunction(callback.onRejected)) {
            koios._promiseResolver.executeCallback(callback.onRejected, callback.promise, this.reason);
        } else {
            callback.promise.reject(this.reason);
        }
    },

    _execute: function(fulfilled) {
        var t = this;
        if (fulfilled) {
            koios.each(this._callbacks, function(theItem) {
                t._fulfill(theItem);
            });
        } else {
            koios.each(this._callbacks, function(theItem) {
                t._reject(theItem);
            });
        }
        this.clearCallbacks();
    },

// =================================================
// Override these for future, repeatable promises

    clearCallbacks: function() {
        this._callbacks.length = 0;
    },

    shouldChangeState: function(callback) {
        if (this.state === koios.Promise.state.PENDING) {
            callback();
        }
    },

    _promise: function() {
        return koios.promise();
    },

    _retVal: function(promise) {
        return promise._thenable();
    },

    _thenable: function() {
        return {then:this.bind("then")};
    },

// =================================================
// Public Resolution

    fulfill: function(value) {
        var t = this;
        this.shouldChangeState(function() {
            t.state = koios.Promise.state.FULFILLED;
            t.value = value;

            koios.defer(koios.bind(t, "_execute", true));
        });

        return t;
    },

    reject: function(reason) {
        var t = this;
        this.shouldChangeState(function() {
            t.state = koios.Promise.state.REJECTED;
            t.reason = reason;

            koios.defer(koios.bind(t, "_execute", false));
        });
    },

    then: function(onFulfilled, onRejected) {
        var promise = this._promise();
        var t = this;

        if (this.state === koios.Promise.state.PENDING) {
            this._callback(onFulfilled, onRejected, promise);
        } else if (this.state === koios.Promise.state.FULFILLED) {
            if (koios.isFunction(onFulfilled)) {
                koios.defer(function() {
                    koios._promiseResolver.executeCallback(onFulfilled, promise, t.value);
                });
            } else {
                promise.fulfill(this.value);
            }
        } else if (this.state === koios.Promise.state.REJECTED) {
            if (koios.isFunction(onRejected)) {
                koios.defer(function() {
                    koios._promiseResolver.executeCallback(onRejected, promise, t.reason);
                });
            } else {
                promise.reject(this.reason);
            }
        }

        return this._retVal(promise);
    },
});

/*
KoiosJS: A Cross-Platform Javascript Application Toolkit
www.koiosjs.com
*/

/*jslint sloppy:true*/
/*jslint nomen:true*/
/*global koios*/

koios.define({
    className: "koios.Object",
    extend: "koios.Class",
    inherit: ["koios.EventSupport", "koios.PropertySupport"],
    accumulated: ["objects"],

    ignoreAlias: true,
    ignoreOwner: false,

    events: ["create", "destroy"],
    objects: [],

    properties: {
		owner: {value: null}
    },

    defaultClass: "koios.Object",

    init: function () {
        this._super("init", arguments);

        this._$ = {};

        this.on("owner:change", this.bind("onOwnerChanged"));
        this.on("owner:beforeChange", this.bind("onBeforeOwnerChanged"));

        if (!this.ignoreOwner) {
            if (!this.owner) {
                this.set("owner", koios.master);
            } else {
                this.set("owner", this.owner);
            }
        }

        this._generateOwnedObjects();
        var t = this;
        koios.defer(function () {
            t.emit("create");
        });
    },

// =================================================
// Private

    _generateOwnedObjects: function () {
        this.addObjects(this.objects);
        this.objects.length = 0;
    },

// =================================================
// Events

    onBeforeOwnerChanged: function () {
        var owner = this.get("owner");
        if (owner) {
            owner.removeObject(this);
        }
    },

    onOwnerChanged: function () {
		var owner = this.get("owner");
        if (owner) {
            koios.log(this.name + " owner changed to " + owner.name, koios.Logger.INTERNAL);
            owner.addObject(this);
        }
    },

// =================================================
// Ownership

    addObject: function (obj) {
        if (this.app) {
            obj.app = this.app;
        }

        if (!koios.isaSubclassOf(obj, "koios.Class")) {
            obj = this.create(obj);
        } else {
            this._$[obj.name] = obj;
        }
        
        return obj;
    },

	addObjects: function (objects) {
        if (!koios.isArray(objects)) {
            objects = [objects];
        }

        var t = this;
        koios.each(objects, function (theItem) {
            t.addObject(theItem);
        });
    },

    create: function (objects, args) {
        if (!koios.isArray(objects)) {
            objects = [objects];
        }

        var obj = [],
            t = this;

        koios.each(objects, function (theObject) {
            koios.mixin(theObject, args);
            var theClass = theObject.className || t.defaultClass,
                Ctor = koios.getConstructor(theClass);

            theObject.className = (koios.isFunction(theClass)) ? theObject.prototype.className : theClass;

            theObject.owner = theObject.owner || t;
            if (!Ctor) {
                return koios["throw"](t.className + ".create() error: could not find constructor for class: " + theClass);
            }

            obj.push(new Ctor(theObject));
        });

        if (obj.length === 1) {
            return obj[0];
        } else {
            return obj;
        }
    },

    removeObject: function (object) {
        if (this._$[object.name]) {
            delete this._$[object.name];
        }
    },

// =================================================
// Searching

    $: function (args) {
        var keys = koios.object(this._$).keys(),
            obj = [],
            t = this,
            i,
            key,
            value;

        if (arguments.length === 0) {
            return koios.object(this._$).values();
        }

        if (koios.isString(args)) {
            return this._$[args];
        }
        
        for (i = 0; i < keys.length; i += 1) {
            obj.push(this._$[keys[i]]);
        }

        for (key in args) {
            if (args.hasOwnProperty(key)) {
                value = args[key];
                obj = koios.array(obj).filter(function (theItem) {
                    if (key.indexOf("{{") !== -1) {
                        return (t.get(key) === value);
                    } else {
                        return (theItem[key] && theItem[key] === value);
                    }
                });
            }
        }

        return obj;
    },

// =================================================
// Garbage Collection

    destroy: function () {
        // Let observers know we're being destroyed
        this.emit("destroy");

        // Set the owner to null so that the ownership chain won't keep the object
        this.set("owner", null);

        // Unbroadcast any events we're part of.
        koios.MessageEmitter.unbroadcast(new RegExp(this.name + ":(.){1,}"));

        // Remove any listeners
        this.removeAllListeners();

        // Destroy down the chain
        koios.each(this.$(), function (theItem) {
            theItem.destroy();
        });

        // Finally have koios.Class clean the rest up.
        this._super("destroy", arguments);
    },

    destroyLater: function () {
        koios.defer(this.bind("destroy"));
    }
});

// =================================================
// We create a koios.master object to hold onto any straggler objects...

koios.master = new koios.Object({name: "koiosMaster", ignoreOwner: true});

/*
KoiosJS: A Cross-Platform Javascript Application Toolkit
www.koiosjs.com
*/

koios.define({
	className:"koios.Controller",
	extend:"koios.Object",
	inherit:"koios.TaskSupport",
	ignoreAlias:true,
	
});
/*
KoiosJS: A Cross-Platform Javascript Application Toolkit
www.koiosjs.com
*/

koios.define({
    className:"koios.Deferred",
    extend: "koios.Promise",
    multipleCallsAllowed: true,

// =================================================

    shouldChangeState: function(callback) {
        if (this.multipleCallsAllowed || this.state === koios.Promise.state.PENDING) {
            callback();
        }
    },

    clearCallbacks: function() {
        if (!this.multipleCallsAllowed) {
            this._callbacks.length = 0;
        }
    },

    _promise: function() {
        return koios.deferred();
    },

    _retVal: function(promise) {
        return promise;
    },

// =================================================

    always: function(callback) {
        this.then(callback, callback);
    },

    delay: function(timeout) {
        var d = koios.deferred();
        d.then(function() {});

        this.then(function(arg) {
            setTimeout(function () {
                d.fulfill(arg);
            }, timeout);
        });

        return d;
    },

    forget: function(deferred) {
        this._callbacks = koios.array(this._callbacks).filter(function(theItem) {
            return theItem.promise._id !== deferred._id;
        });
    },

    error: function(callback) {
        return this.then(null, callback);
    },

    success: function(callback) {
        return this.then(callback, null);
    },
});

/*
KoiosJS: A Cross-Platform Javascript Application Toolkit
www.koiosjs.com
*/

koios.define({
	className:"koios.Queue",
	extend:"koios.Class",
	inherit:["koios.PropertySupport", "koios.EventSupport"],
	ignoreAlias: true,

	priorityLevel: -1,
	defaultPriority: 5,

	properties:{
		priorityLevel: {
			value:-1,
			validate:{set:koios.isNumber}
		}
	},

	init: function() {
		this._super("init", arguments);

		this.tasks = [];
		this._taskMap = {};
		this.stopped = false;
		this.taskCount = 0;

		this.on("priorityLevel:change", this.bind("priorityLevelChanged"));
		this.set("priorityLevel", this.priorityLevel);
		for (var i=0;i<10;i++) {
			this.tasks.push([]);
		}
	},

// =================================================
// Task Manipulation

	addTask: function(taskName, priority, fn) {		
		var task = {fn:fn, name:taskName, priority:priority-1};
		this.tasks[task.priority].push(task);
		this._taskMap[taskName] = task;
		this.taskCount++;

		var level = this.get("priorityLevel");
		if (level < task.priority) {
			this.set("priorityLevel", task.priority);
		}
	},

	removeTask: function() {

	},

// =================================================
// Queue Manipulation

	start: function() {
		if (this.stopped) {
			this.stopped = false;
			this._next();
		}
	},

	stop: function() {
		this.stopped = true;
	},

// =================================================
// Queue Execution

	_next: function() {
		if (!this.stopped) {
			setTimeout(this.bind("_onTick"), 10);
		}
	},

	_onTick: function() {
		var level = this.get("priorityLevel");
		// console.log("tick - "+level);
		var tasks = this.tasks[level];
		var task = tasks.shift();
		if (task) {
			this.taskCount--;
			this._taskMap[task.name] = null;
			task.fn();
		}

		this._setNextPriorityLevel();

		if (this.taskCount > 0) {
			this._next();
		} else {
			this._queueDone();
		}
	},

	_setNextPriorityLevel: function() {
		var level = this.get("priorityLevel");
		for(var i = level; i >= 0; i--) {
			if (this.tasks[i].length > 0) {
				this.set("priorityLevel", i);
				return;
			}
		}
	},

	_start: function() {
		this._next();
	},

	_queueDone: function() {
		this.set("priorityLevel", -1);
		this._taskMap = {};
	},
	
// =================================================
// Events

	priorityLevelChanged: function() {
		var level = this.get("priorityLevel");
		if (this.priorityLevel === -1 && level > -1) {
			this._start();
		}
		this.priorityLevel = level;
	},
});

koios.bgQueue = new koios.Queue();
/*
KoiosJS: A Cross-Platform Javascript Application Toolkit
www.koiosjs.com
*/

koios.define({
    className:"koios.Task",
    extend:"koios.Class",
    inherit:"koios.CloneSupport",

    statics:{
        priority: {
            LOW:1,
            MEDIUM:5,
            HIGH:10,
        }
    },

    init: function() {
        this._super("init", arguments);

        this._clonable = ["className", "name", "method", "owner", "priority", "params"];
    },

    _fn: function() {
        var t = this;
        var args = koios.argumentsToArray(arguments);
        return function() {
            t.execute.apply(t, args);
        }
    },

    execute: function() {
        var args = koios.argumentsToArray(arguments);
        if (koios.isString(this.method)) {
            this.owner[this.method].apply(this.owner, args);
        } else if (koios.isFunction(this.method)) {
            this.method.apply(this.owner, args);
        }
    },

    enqueue: function() {
        if (!this.queue) {
            this.queue = koios.bgQueue;
        }
        if (!("priority" in this)) {
            this.priority = koios.Task.priority.MEDIUM;
        }

        this.queue.addTask(this.name, this.priority, this._fn.apply(this, koios.argumentsToArray(arguments)));

        return this;
    },

    setPriority: function(newPriority) {
        this.priority = newPriority;
        return this;
    }
});

koios.task = function(defn) {
    if (koios.isFunction(defn)) {
        return new koios.Task({method:defn});
    } else {
        return new koios.Task(defn);
    }
};
/*
KoiosJS: A Cross-Platform Javascript Application Toolkit
www.koiosjs.com
*/

koios.define({
	className: "koios.Collection",
	extend: "koios.Class",
	inherit: ["koios.EventSupport"],

	modelName: "",

	init: function() {
		this._super("init", arguments);
		this.records = [];

		if (this.modelName !== "" && !this._cm) {
			this.emit("koios.Collection::create", this);
		}

		koios.on(this.modelName+"::create", this.bind("modelAdded"));
		if (this.dataSource) {
			this._setDataSource();
		}
	},

// =================================================
// Private

	_setDataSource: function() {
		if (koios.isObject(this.dataSource) && koios.isaSubclassOf(this.dataSource, "koios.Store")) {
			this.dataSource = this.create(this.dataSource);
		}
	},

// =================================================
// Events

	modelAdded: function(theModel) {
		var t = this;
		var add = true;

		this.records.push(theModel);
		this.emit("add", theModel);

		theModel.on("change", this.bind("modelChanged"));
		theModel.on("destroy", this.bind("modelDestroyed"));
	},

	modelDestroyed: function(theModel) {
		koios.array(this.records).remove(theModel);
		this.emit("remove", theModel);
	},

	modelChanged: function(theModel) {
		this.emit("change", this, theModel);
	},

});
/*
KoiosJS: A Cross-Platform Javascript Application Toolkit
www.koiosjs.com
*/

koios.singleton({
	className:"koios.CollectionManager",
	extend: "koios.Class",

	init: function() {
		this._super("init", arguments);

		this._col = {};
		koios.on("koios.Collection::create", this.bind("collectionAdded"));
	},

// =================================================
// Retrieval

	collectionForModel: function(modelName) {
		if (this._col[modelName]) {
			return this._col[modelName];
		}

		var ctor = koios.getConstructor(modelName);
		if (koios.isEmpty(ctor)) {
			return koios.throw("koios.CollectionManager.collectionForModel error: invalid modelName: "+modelName);
		}
		
		return this._col[modelName] = koios.Collection.create({modelName:modelName, _cm:true});
	},

// =================================================
// Events

	collectionAdded: function(theCollection) {
		this._col[theCollection.modelName] = theCollection;
	},

})
/*
KoiosJS: A Cross-Platform Javascript Application Toolkit
www.koiosjs.com
*/

koios.define({
	className: "koios.Filter",
	extend: "koios.Class",
	inherit:["koios.EventSupport"],

	filter: {},
	modelName: "",

	init: function() {
		this._super("init", arguments);
		this.records = [];
		this._listeners = {};

		if (this.modelName && this.modelName !== "") {
			this.collection = koios.CollectionManager.collectionForModel(this.modelName);
		}

		if (!this.collection) {
			return koios.throw("koios.Filter error: no collection associated for model: "+this.modelName);
		}

		this._setupMessages();
		this._fetch();
	},

// =================================================
// Setup

	_setupMessages: function() {
		this.collection.on("add", this.bind("modelAdded"));
		this.collection.on("remove", this.bind("modelRemoved"));
		this.collection.on("change", this.bind("collectionChanged"));
	},

	_fetch: function() {
		var posRecords = this.collection.records;

		this.records = [];
		for (var i=0;i<posRecords.length;i++) {
			var record = posRecords[i];
			var add = this._recordMatchesFilter(record);
			if (add) {
				this._addRecord(record);
			}
		}
	},

// =================================================
// Record Manipulation

	_recordMatchesFilter: function(record) {
		var add = true;
		koios.object(this.filter).forEach(function(key, value) {
			if (add) {
				if (koios.isFunction(value)) {
					// Filtering function
					add = value(record.get(key));
				} else if (koios.isRegex(value)) {
					// Filtering via Regex
				} else if (record.get(key) !== value) {
					add = false;
				}
			}
		});
		return add;
	},

	_addRecord: function(record) {
		this.records.push(record);
		this._listeners[record.name] = [];
		this._listeners[record.name].push(record.on("change", this.bind("modelChanged")));
		this.emit("add", record);
	},

	_removeRecord: function(record) {
		koios.each(this._listeners[record.name], function(theListener) {
			record.off(theListener);			
		});
		this._listeners[record.name].length = 0;
		koios.array(this.records).remove(record);
		this.emit("remove", record);
	},

// =================================================
// Model Messages

	modelAdded: function(theModel) {
		if (this._recordMatchesFilter(theModel)) {
			this._addRecord(theModel);
		}
	},

	modelChanged: function(theModel) {
		this.emit("change", theModel);
		if (!this._recordMatchesFilter(theModel)) {
			this._removeRecord(theModel);
		}
	},

	modelRemoved: function(theModel) {
		this._removeRecord(theModel);
	},

	collectionChanged: function(theCollection, theModel) {
		if (this._recordMatchesFilter(theModel) && koios.indexOf(this.records, theModel) === -1) {
			this._addRecord(theModel);
			this.emit("change", theModel);
		}
	},

// =================================================
// Garbage Collection

	destroy: function() {
		this._super("destroy", arguments);
		this.emit("destroy", this);
		koios.MessageEmitter.unbroadcast(new RegExp(this.name+":(.){1,}"));
	}

});
/*
KoiosJS: A Cross-Platform Javascript Application Toolkit
www.koiosjs.com
*/

koios.define({
	className: "koios.OldModel",
	extend: "koios.Class",
	inherit: ["koios.PropertySupport", "koios.JSONSupport", "koios.EventSupport"],

	limitProperties: false,
	ready: false,

	statics: {
		_kid: 0,
		_getID: function() {
			return this._kid++;
		}
	},

	constructor: function() {
		this._super("_ct");
		this.ready = false;
	},

	init: function() {
		this._super("init", arguments);
		this._stringable = this.allProperties;
		this._storeRef = [];

		this.set = koios.wrap(this.bind("set"), this.bind("_beforeSet"));
		var inVal = arguments[0];
		if (koios.isString(inVal)) {
			this.set("value", inVal);
		} else {
			var t = this;
			koios.object(inVal).forEach(function(key, value) {
				t.set(key, value);
			});
		}

		if (!this.get("_id")) {
			this.set("_id", koios.Model._getID());
		}

		koios.emit(this.className, "create", this);
		this.ready = true;
	},

// =================================================
// Custom set() method (which we can't override, because it's being inherited via PropertySupport - we need to wrap the method)

	_beforeSet: function(setFn, propName, propVal) {
		if (koios.isString(propName) && arguments.length === 2) {
			return this.set("value", propName);
		}

		if (!this._props[propName]) {
			if (!this.limitProperties || propName === "_id") {
				this.addProperty(propName, null);
			} else {
				return koios.warn(this.className+".set() warning: no property "+propName+" defined");
			}
		}

		setFn(propName, propVal);
		this.emit("change", this);
	},

// =================================================
// Additional Property Methods

	empty: function(propName) {
		if (propName) {
			this.set(propName, null);
		} else {
			var props = this.allProperties();
			var t = this;
			koios.each(props, function(theItem) {
				if (theItem !== "_id") {
					t.set(theItem, null);
				}
			});
		}
	},

	resetAll: function() {
		var props = this.allProperties();
		var t = this;
		koios.each(props, function(theItem) {
			if (theItem !== "_id") {
				t.reset(theItem);
			}
		});
	},

// =================================================
// Saving
	
	remove: function(onSuccess, onError) {
		// We separate this from destroy() because we might not want to kill of the server side of things just because we want
		// to dereference the object in the JS side.
		var stores = this._ctor.stores;
		if (!stores || stores.length === 0) {
			return;
		}
		for (var i=0;i<this._ctor.stores.length;i++) {
			var store = this._ctor.stores[i];
			store.remove(this, onSuccess, onError);
		}

		koios.defer(this.bind("destroy"));
	},
	
	save: function(onSuccess, onError) {
		var stores = this._ctor.stores;
		if (!stores || stores.length === 0) {
			return koios.warn(this.className+".save() warning: no stores attached.");
		}
		for (var i=0;i<this._ctor.stores.length;i++) {
			var store = this._ctor.stores[i];
			var action = store._actionForItem(this);
			store[action](this, onSuccess, onError);
		}
		return this;
	},

// =================================================
// Garbage Collection
	
	destroy: function() {
		this._super("destroy", arguments);
		this.emit("destroy", this);
        koios.MessageEmitter.unbroadcast(new RegExp(this.name+":(.){1,}"));
	},

	storeDestroyed: function(theStore) {
		koios.array(this._storeRef).remove(theStore);
	},

});
/*
KoiosJS: A Cross-Platform Javascript Application Toolkit
www.koiosjs.com
*/

koios.define({
	className:"koios.Model",
	extend:"koios.Class",
	inherit:["koios.EventSupport", "koios.JSONSupport", "koios.PropertySupport"],

	deferred: false,
	limitToSchema: true,
	schema: {},
	created: false,
	keyPath: "id",
	silenced: false,

	statics: {
		_kid: 0,
		_getID: function() {
			return this._kid++;
		}
	},

	constructor: function() {
		// Lop off the constructor arguments because we'll define them ourselves.
		this._super("_ct");
		this.ready = false;
	},

	init: function() {
		this._super("init", arguments);
		this._stringable = this.allProperties;
		this._storeRef = [];
		this.silenced = true;

		this.emit = koios.wrap(this.bind("emit"), this.bind("_beforeEmit"));
		this.set = koios.wrap(this.bind("set"), this.bind("_beforeSet"));

		var inVal = arguments[0];
		if (koios.isString(inVal)) {
			this.set("value", inVal);
		} else {
			var t = this;
			koios.object(inVal).forEach(function(key, value) {
				t.set(key, value);
			});
		}

		if (!this.get(this.keyPath)) {
			this._defineKeyPath();
		}

		if (!this.deferred) {
			this.created();
		}
	},

// =================================================
// Private

	_defineKeyPath: function() {
		this.set(this.keyPath, koios.Model._getID());
	},

	_beforeSet: function(setFn, propName, propVal) {
		if (koios.isString(propName) && arguments.length === 2) {
			return this.set("value", propName);
		}

		if (!koios.isEmpty(this.schema)) {
			if (propName === this.keyPath) {
				if (!this._props[propName]) {
					this.addProperty(propName, null);
				}
			} else {
				var validator = this.schema[propName];
				var shouldSet = true;
				if (validator) {
					shouldSet = validator(propVal);
				}

				if (!this._props[propName]) {
					if ((propName in this.schema) || !this.limitToSchema) {
						this.addProperty(propName, null);
					}
				}
			}
		}

		setFn(propName, propVal);
		this.emit("change", this);
	},

	_beforeEmit: function(setFn) {
		var args = Array.prototype.slice.call(arguments, 1);
		if (!this.silenced) {
			setFn.apply(this, args);
		}
	},

// =================================================

	created: function() {
		this.deferred = false;
		this.silenced = false;
		koios.emit(this.className, "create", this);
	}
});
/*
KoiosJS: A Cross-Platform Javascript Application Toolkit
www.koiosjs.com
*/

koios.define({
	className:"koios.Store",
	extend: "koios.Class",
	inherit: ["koios.EventSupport"],

	models:{},

	statics: {
		_typeMap: {},
		type: {
			LOCAL: "local",
			REMOTE: "remote",
			SYNC: "sync",
		},
		storeWithType: function(type) {
			if (this._typeMap[type]) {
				return this._typeMap[type];
			}

			return this;
		}
	},

	init: function() {
		this._super("init", arguments);
		this._storeAdaptors = {};
		var t = this;

		koios.object(this.models).forEach(function(key, value) {
			console.log(key);
			var ctor;
			if (!value.className) {
				ctor = koios.Store.storeWithType(value.type);
			} else {
				ctor = koios.getConstructor(value.className);
			}
			delete value.type;
			value.modelName = key;

			if (ctor && !koios.isEmpty(ctor)) {
				t._storeAdaptors[key] = new ctor(value);
			}
		});
	},

// =================================================

	_handle: function(action, modelName, args) {
		var store = this.storeForModel(modelName);
		if (store) {
			store[action].apply(store, args);
		}
	},

	storeForModel: function(modelName) {
		return this._storeAdaptors[modelName];
	},

	findAll: function(modelName) {
		var args = Array.prototype.slice.call(arguments, 1);
		this._handle("findAll", modelName, args);
	},

	// add: function() {
	// 	koios.warn("koios.Store warning: this is an abstract placeholder class");
	// },

	// find: function() {
	// 	koios.warn("koios.Store warning: this is an abstract placeholder class");
	// },

	// findAll: function() {
	// 	koios.warn("koios.Store warning: this is an abstract placeholder class");
	// },

	// findOne: function() {		
	// 	koios.warn("koios.Store warning: this is an abstract placeholder class");
	// },

	// update: function() {
	// 	koios.warn("koios.Store warning: this is an abstract placeholder class");
	// },

	// remove: function() {
	// 	koios.warn("koios.Store warning: this is an abstract placeholder class");
	// },

// =================================================
// Garbage Collection

	destroy: function() {
		this._super("destroy", arguments);
		this.emit("destroy", this);
		koios.MessageEmitter.unbroadcast(new RegExp(this.name+":(.){1,}"));
	}

});
/*
KoiosJS: A Cross-Platform Javascript Application Toolkit
www.koiosjs.com
*/

if (koios.strategy['device'] === "window") {

koios.singleton({
    className:"koios.Device",
    extend:"koios.Class",
    inherit:["koios.JSONSupport"],

    _stringable:['vendor', 'userAgent', 'language', 'osName', 'osVersion', 'hasTransform', 'hasTouch', 'hasGestures', 'has3d'],
    
    vendor: (/webkit/i).test(navigator.appVersion) ? 'webkit' :
        (/firefox/i).test(navigator.userAgent) ? 'moz' :
        (/trident/i).test(navigator.userAgent) ? 'ms' :
        'opera' in window ? 'O' : '',

    _browser: (/chrome/i).test(navigator.userAgent) ? "Chrome" :
        (/safari/i).test(navigator.userAgent) ? "Safari" :
        (/AppleWebKit/i).test(navigator.userAgent) ? "Safari" :
        (/mozilla/i).test(navigator.userAgent) ? "Firefox" :
        (/msie/i).test(navigator.userAgent) ? "Internet Explorer" :
        (/opera/i).test(navigator.userAgent) ? "Opera" : "",

    userAgent: navigator.userAgent,

    language: navigator.language,

    _os: {
        android: "Android",
        ios: "iOS",
        playbook: "BlackBerry Playbook",
        blackberry: "BlackBerry",
        bb10: "BlackBerry 10",
        webos: "webOS",
        mac: "Macintosh",
        windows: "Windows",
        // windows phone
    },

    isAndroid: (/android/gi).test(navigator.appVersion),
    isIOS: (/iphone|ipad/gi).test(navigator.appVersion),
    isPlaybook: (/playbook/gi).test(navigator.appVersion),
    isTouchPad: (/hp-tablet/gi).test(navigator.appVersion),
    iswebOS: (/webos/gi).test(navigator.appVersion),
    isBlackberry: (/blackberry/gi).test(navigator.appVersion),
    isMac: (/macintosh/gi).test(navigator.appVersion),
    isWindows: (/windows/gi).test(navigator.appVersion),

    init: function() {
        this._super("init", arguments);

        this.hasTransform = this.vendor + 'Transform' in document.documentElement.style;
        this.hasTouch = Boolean(("ontouchstart" in window) || window.navigator.msMaxTouchPoints);
        this.hasGestures = Boolean(("ongesturestart" in window) || window.navigator.msMaxTouchPoints);
        this.has3d ='WebKitCSSMatrix' in window && 'm11' in new WebKitCSSMatrix();      
 
        this.browser = this._getBrowser();
        this._getOS();
    },

    os: function() {
        return {osVersion:this.osVersion, osName:this.osName};
    },

    _getOS: function() {
        if (koios.config('simulatesOS') && koios.config('simulatesOS') !== "") {
            this.osName = koios.config('simulatesOS');
            this.osVersion = "Unknown";
            return;
        }

        if (this.isAndroid) {
            this.osName = this._os.android;
            // This may be very dangerous, as there's a chance that ROMs and such are going to mess with
            // the UserAgent string...
            this.osVersion = this.userAgent.split('Android ')[1].split(';')[0];
        }
        if (this.isIOS) {
            this.osName = this._os.ios;
            this.osVersion = this.userAgent.split("OS ")[1].split(' like')[0].replace(/\_/g, ".");
        }
        if (this.isMac) {
            this.osName = this._os.mac;
            this.osVersion = this.userAgent.split("OS X ")[1].split(")")[0].replace(/\_/g, ".");
            if (this.osVersion.indexOf(';') > -1) {
                this.osVersion = this.osVersion.split(';')[0];  // Firefox adds the rv code to OS X
            }
        }
        if (this.isPlaybook) {
            this.osName = this._os.playbook;
        }
        if (this.isBlackberry) {
            this.osName = this._os.blackberry;
        }
        if (this.iswebOS) {
            this.osName = this._os.webos;
        }
    },    

    _getBrowser: function() {
        var ret = {};
        var verOffset;

        ret.name = this._browser;
        if (this._browser === "Chrome") {
            verOffset = this.userAgent.indexOf("Chrome") + 7;
            ret.version = parseInt(navigator.userAgent.substring(verOffset), 10);
        } else if (this._browser === "Safari") {
            verOffset = this.userAgent.indexOf('Safari') + 7;
            ret.version = parseInt(navigator.userAgent.substring(verOffset), 10);
            if (this.userAgent.indexOf('Version') > -1) {
                verOffset = this.userAgent.indexOf('Version') + 8;
                ret.version = parseInt(navigator.userAgent.substring(verOffset), 10);
            }
            if (this.userAgent.indexOf('iPhone OS') > -1) {
                verOffset = this.userAgent.indexOf('Version') + 9;
                ret.version = parseInt(navigator.userAgent.substring(verOffset), 10);               
            }
        } else if (this._browser === "Firefox") {
            verOffset = this.userAgent.indexOf('Firefox') + 8;
            ret.version = parseInt(navigator.userAgent.substring(verOffset), 10);
        } else {
            ret.version = parseInt(navigator.appVersion, 10);
        }
        return ret;
    },

});

} else {

koios.singleton({
    className:"koios.Device",
    extend:"koios.Class",
    inherit:["koios.JSONSupport"],

    init: function() {
        this._super("init", arguments);

        // TODO: Implement koios.Device for the server-side
    }
});

}

/*
KoiosJS: A Cross-Platform Javascript Application Toolkit
www.koiosjs.com
*/

koios.singleton({
    className:"koios.LocalStorage",
    extend: "koios.Class",

    init: function() {
        this._super("init", arguments);
        this._setStorage();

        koios.MessageEmitter.broadcast("koios:storage");
        koios.EventHandler.listen("storage", this.bind("onStorage"));
    },

// =================================================
// Private

    _setStorage: function() {
        if (!this.localStorage) {
            if (typeof window !== "undefined" && window.localStorage) {
                this.localStorage = window.localStorage;
            }
        }
    },

// =================================================
// Events

    onStorage: function(event) {
        koios.MessageEmitter.emit("koios:storage", event);
    },

// =================================================
// Public

    clear: function() {
        this.localStorage.clear();
    },

    get: function(key) {
        return this.localStorage.getItem(key);
    },

    remove: function(key) {
        if (!key) {
            this.clear();
        } else {
            this.localStorage.removeItem(key);
        }
    },

    set: function(key, value) {
        this.localStorage.setItem(key, value);
        this.onStorage(); // we emit koios:storage even for local apps...
    },

});

/*
KoiosJS: A Cross-Platform Javascript Application Toolkit
www.koiosjs.com
*/

(function(me) {
	me.broadcast("koios:ready");
	if (koios.Device.hasWindow) {
		koios.EventHandler.listen("DOMContentLoaded", function(event) {
			me.emit("koios:ready");
			me.unbroadcast("koios:ready");
		}, document);
	} else {
		koios.defer(function() {
			me.emit("koios:ready");
			me.unbroadcast("koios:ready");
		});
	};

	koios.ready = function(listener) {
		if (koios.loaded) {
			listener();
		} else {
			me.on("koios:ready", listener);	
		}
	};

	koios.ready(function() {
		koios.loaded = true;
	});

})(koios.MessageEmitter);

/*
KoiosJS: A Cross-Platform Javascript Application Toolkit
www.koiosjs.com
*/

/*jslint sloppy:true*/
/*jslint nomen:true*/
/*global koios*/


koios.define({
    className: "koios.Application",
    extend: "koios.Object",
    ignoreAlias: true,
    
    init: function () {
        this._super("init", arguments);
    }
});

})(this);
