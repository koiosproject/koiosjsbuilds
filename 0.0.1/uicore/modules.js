(function(global) {
/*
KoiosJS: A Cross-Platform Javascript Application Toolkit
www.koiosjs.com
*/

/*jslint sloppy:true*/
/*jslint nomen:true*/
/*global koios, module*/

var koiosUI = {};

if (typeof module !== "undefined" && module.exports) {
    module.exports = koiosUI;
} else {
    window.koiosUI = koiosUI;
}

/*
KoiosJS: A Cross-Platform Javascript Application Toolkit
www.koiosjs.com
*/

/*jslint sloppy:true*/
/*jslint nomen:true*/
/*global koios*/

koios.define({
    className: "koiosUI.Drawable",
    extend: "koios.Object",
    
    attrs: {},
    views: [],
    tag: "div",
    defaultDrawable: "koiosUI.View",
    
    init: function () {
        this._super("init", arguments);
        
        if (!this.domNode) {
            this._createDomNode();
        }
    },
    
    _createDomNode: function () {
        this.domNode = koios.el(this.tag);
    },
    
    renderInto: function (domNode) {
        koios.append(domNode, this.domNode);
        this.emit("rendered");
        return this;
    }
});

/*
KoiosJS: A Cross-Platform Javascript Application Toolkit
www.koiosjs.com
*/

/*jslint sloppy:true*/
/*jslint nomen:true*/
/*global koios*/

koios.define({
    className: "koiosUI.View",
    extend: "koiosUI.Drawable",
    accumulated: ["classes", "styles", "views"],
    
    properties: {
        flex: {value: 1}
    },
    
    cssClass: "",
    cssStyle: "",
    flex: 1,
    innerHTML: "",
    
// =================================================

    constructor: function () {
        this.classes = koios.mixin([], this.classes);
        this.styles = koios.mixin({}, this.styles);
        
        this._super("_ct", arguments);
    },
    
    init: function () {
        this._super("init", arguments);
        
        // Set Attrs
        this._setId();
        this._setAttrs();
        this.attr("koios-view", this.className);
        
        // Set Styles
        this._setCssClass();
        this._setCssStyle();
        this.html(this.innerHTML);
        
        // Render Owned Views
        this._renderOwnedView();
        
        this.on("owner:change", this.bind("_viewOwnerChanged"));
    },
    
// =================================================
// Attributes
    
    _setId: function () {
        if (!this.domId) {
            this.attr("id", this.name);
        } else {
            this.attr("id", this.domId);
        }
    },
    
    _setAttrs: function () {
        var key;
        
        for (key in this.attrs) {
            if (this.attrs.hasOwnProperty(key)) {
                this.attr(key, this.attrs[key]);
            }
        }
    },
    
    attr: function (key, value) {
        koios.attr(this.domNode, key, value);
    },

    html: function (theHTML) {
        koios.html(this.domNode, theHTML);
    },
    
// =================================================
// CSS Classes
    
    _setCssClass: function () {
        var classes = (koios.isString(this.cssClass)) ? this.cssClass.split(",") : this.cssClass,
            i,
            item;
                
        for (i = 0; i < classes.length; i += 1) {
            item = koios.string(classes[i]).trimWhitespace();
            if (item !== "") {
                this.addClass(item);
            }
        }
    },

    addClass: function (theClass) {
        koios.addClass(this.domNode, theClass);
    },
    
    removeClass: function (theClass) {
        koios.removeClass(this.domNode, theClass);
    },
    
    toggleClass: function (theClass) {
        koios.toggleClass(this.domNode, theClass);
    },
    
    allClasses: function () {
        return koios.allClasses(this.domNode);
    },
    
// =================================================
// CSS Styles
    
    _setCssStyle: function () {
        if (koios.isString(this.cssStyle)) {
            koios.attr(this.domNode, "style", this.cssStyle);
        } else {
            this.setStyle(this.cssStyle);
        }
    },
    
    getStyle: function (key) {
        return koios.css(this.domNode, key);
    },

    removeStyle: function (key) {
        koios.removeCss(this.domNode, key);
    },
    
    setStyle: function (key, value) {
        koios.css(this.domNode, key, value);
    },
    
    style: function () {
        return koios.css(this.domNode);
    },
        
// =================================================
// Ownership
    
    _viewOwnerChanged: function () {
        var owner = this.get("owner");
        if (!owner) {
            koios.remove(this.domNode.parentNode, this.domNode);
        }
    },

// =================================================
// Subviews
    
    _renderOwnedView: function () {
        var i, allViews = [];
        for (i = 0; i < this.views.length; i += 1) {
            allViews.push(this._insertView(this.views[i]));
        }
        
        this.views = allViews;
    },
    
    _insertView: function (theView) {
        if (!theView.className) {
            theView.className = this.defaultDrawable;
        }
        
        var item = this.addObject(theView);
        item.set("owner", this);
        item.renderInto(this.domNode);
        return item;
    },
    
    addView: function (theView) {
        var item = this._insertView(theView);
        this.views.push(item);
        return item;
    },
    
    moveView: function (theView, theIndex) {
    },
    
    removeView: function (theView) {
    },
    
// =================================================
// Templating
    
    template: function (theTemplate) {
        this.innerHTML = koios.template(this.domNode.innerHTML, theTemplate);
        this.html(this.innerHTML);
    },
    
// =================================================
// Cleanup
    
    destroy: function () {
        this.set("owner", null);
        this._super("destroy", arguments);
    }
});

/*
KoiosJS: A Cross-Platform Javascript Application Toolkit
www.koiosjs.com
*/

/*jslint sloppy:true*/
/*jslint nomen:true*/
/*global koios*/

koios.define({
    className: "koiosUI.AjaxFragment",
    extend: "koiosUI.View",

    endpoint: null,
    template: null,
    deferred: false,
    
    init: function () {
        this._super("init", arguments);
    
        if (this.endpoint && !this.deferred) {
            koios.defer(this.bind("reload"));
        }
    },

// =================================================
// Templating
    
    _template: function (theObject) {
        this.html(koios.template(this._nodeInnerHTML, theObject));
    },
    
    reload: function (endpoint, template) {
        if (arguments.length === 0) {
            endpoint = this.endpoint;
        }
        
        if (!koios.isString(endpoint)) {
            template = endpoint;
            endpoint = this.endpoint;
        }
        
        if (!endpoint) {
            return;
        }

        if (!template) {
            template = this.template;
        }
        
        var t = this;
        koios.Ajax.GET(endpoint).then(function (response) {
            t._nodeInnerHTML = response.text;
            t.refresh(template);
        });
    },
    
    refresh: function (templateObject) {
        if (templateObject) {
            this._template(templateObject);
        } else {
            this.html(this._nodeInnerHTML);
        }
    }
});
/*
KoiosJS: A Cross-Platform Javascript Application Toolkit
www.koiosjs.com
*/

/*jslint sloppy:true*/
/*jslint nomen:true*/
/*global koios*/

/*
A fragment is a KoiosUI view containing some form of outside, or deferred
content. It can either be loaded via Ajax, or from an existing set of DOM
elements.
*/

koios.define({
    className: "koiosUI.Fragment",
    extend: "koiosUI.View",

    targetNode: null,
    template: null,
    
    init: function () {
        this._super("init", arguments);
    
        if (this.targetNode) {
            koios.defer(this.bind("_renderNode"));
        } else {
            koios.warn("koiosUI.Fragment warning: no targetNode set for " + this.name);
        }
    },

    _renderNode: function () {
        var node = koios.query(this.targetNode, true);
        
        koios.append(this.domNode, node);
        this._nodeInnerHTML = this.domNode.innerHTML;
        
        if (this.template) {
            this._template(this.template);
        }
    },

// =================================================
// Templating
    
    _template: function (theObject) {
        this.html(koios.template(this._nodeInnerHTML, theObject));
    },
    
    
    refresh: function (templateObject) {
        if (arguments.length > 0) {
            this._template(templateObject);
        }
    }
});
})(this);
